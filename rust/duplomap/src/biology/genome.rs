use std::collections::{HashMap, HashSet};
use std::io::{Read, Seek};
use std::iter;
use std::marker::Sync;
use std::ops::Deref;
use std::cmp::{min, Ordering};
use std::io::{Write, Result};
use std::path::Path;
use std::fmt::{Debug, Formatter};

use rand::thread_rng;
use rand::seq::SliceRandom;
use bio::io::fasta::{IndexedReader, Record};
use bio::utils::{Text, TextSlice};
use bio::alphabets;

use biology::spans::Span;

pub trait Fetcher: Sync {
    fn fetch(&mut self, chrom_id: u32, start: u32, end: u32) -> Option<Text>;

    fn fetch_unchecked(&mut self, chrom_id: u32, start: u32, end: u32) -> Text {
        match self.fetch(chrom_id, start, end) {
            Some(value) => value,
            _ => panic!("Failed to fetch chrom_id={}:{}-{}", chrom_id, start, end),
        }
    }
}

impl<R: Read + Seek + Sync> Fetcher for IndexedReader<R> {
    fn fetch(&mut self, chrom_id: u32, start: u32, end: u32) -> Option<Text> {
        self.fetch_by_rid(chrom_id as usize, start as u64, end as u64).ok()?;
        let mut res = Vec::new();
        self.read(&mut res).ok()?;
        Some(res)
    }
}

struct SequenceVec(Vec<Text>);

impl Fetcher for SequenceVec {
    fn fetch(&mut self, chrom_id: u32, start: u32, end: u32) -> Option<Text> {
        Some(self.0.get(chrom_id as usize)?[start as usize..end as usize].to_owned())
    }
}

pub struct Genome {
    reader: Box<dyn Fetcher + Send>,
    names: Vec<String>,
    lengths: Vec<u32>,
    ids: HashMap<String, u32>,
}

impl Genome {
    pub fn from_file<P: AsRef<Path>>(filename: P) -> Self {
        let filename = filename.as_ref();
        let reader = match IndexedReader::from_file(&filename) {
            Ok(value) => Box::new(value),
            _ => panic!("Failed to load genome and its index from \"{}\"", filename.display()),
        };

        let n_seqs = reader.index.sequences().len();
        let mut names = Vec::with_capacity(n_seqs);
        let mut lengths = Vec::with_capacity(n_seqs);
        let mut ids = HashMap::with_capacity(n_seqs);
        for (id, sequence) in reader.index.sequences().iter().enumerate() {
            names.push(sequence.name.clone());
            lengths.push(sequence.len as u32);
            ids.insert(sequence.name.clone(), id as u32);
        }

        Genome { reader, names, lengths, ids }
    }

    pub fn from_sequence(sequence: TextSlice) -> Self {
        let record = Record::with_attrs(&"1", None, sequence);
        Genome::from_sequences(iter::once(record))
    }

    pub fn from_sequences<I>(records: I) -> Self
    where I: Iterator<Item = Record>,
    {
        let (names, sequences): (Vec<_>, Vec<_>) = records
            .map(|rec| (rec.id().to_string(), rec.seq().to_vec())).unzip();
        let lengths = sequences.iter().map(|seq| seq.len() as u32).collect();
        let ids = names.iter().cloned().enumerate().map(|(id, name)| (name, id as u32)).collect();
        let reader = Box::new(SequenceVec(sequences));
        Genome { reader, names, lengths, ids }
    }

    pub fn chrom_name(&self, id: u32) -> &str {
        &self.names[id as usize]
    }

    pub fn chrom_id(&self, name: &str) -> Option<u32> {
        self.ids.get(name).copied()
    }

    pub fn chrom_len(&self, id: u32) -> u32 {
        self.lengths[id as usize]
    }

    pub fn count_chromosomes(&self) -> usize {
        self.names.len()
    }

    pub fn fetch(&mut self, span: &impl Span) -> Text {
        let chrom_id = span.chrom_id();
        debug_assert!((chrom_id as usize) < self.names.len(),
                      "Chrom id is too big ({} >= {})", chrom_id, self.names.len());
        let end = span.end();
        assert!(end <= self.lengths[chrom_id as usize],
            "Region end is longer than the chromosome ({} > {})", end,
            self.lengths[chrom_id as usize]);

        let mut text = self.reader.fetch_unchecked(chrom_id, span.start(), end);
        standartize(&mut text);
        if span.strand() {
            text
        } else {
            text.reverse_complement()
        }
    }
}

pub fn generate(size: usize) -> Text {
    let symbols = b"ACGT";
    let mut rng = thread_rng();
    (0..size).map(|_| *symbols.choose(&mut rng).unwrap()).collect()
}

pub(crate) fn nt_to_int(nt: u8) -> u32 {
    match nt {
        b'A' => 0,
        b'C' => 1,
        b'G' => 2,
        b'T' => 3,
        b'U' => 3,
        _ => panic!("Unexpected nucleotide: {}", nt as char),
    }
}

fn int_to_nt(value: u32) -> u8 {
    match value {
        0 => b'A',
        1 => b'C',
        2 => b'G',
        3 => b'T',
        _ => panic!("Unexpected nucleotide number: {}", value)
    }
}

fn standartize_nt(nt: &mut u8) {
    nt.make_ascii_uppercase();
    match nt {
        b'A' | b'C' | b'G' | b'T' | b'N' => {},
        b'U' => {
            *nt = b'T';
        },
        b'W' | b'S' | b'M' | b'K' | b'R' | b'Y'
                | b'B' | b'D' | b'H' | b'V' | b'Z' => {
            warn!("Replacing nucleotide {} with N", *nt as char);
            *nt = b'N';
        },
        _ => panic!("Unexpected nucleotide: {}", *nt as char),
    }
}

pub fn standartize(text: &mut Text) {
    text.iter_mut().for_each(|nt| standartize_nt(nt));
}

pub trait Sequence: Deref<Target = [u8]> {
    fn reverse_complement(&self) -> Text {
        alphabets::dna::revcomp(self.deref())
    }

    fn complexity(&self) -> f64 {
        let n = self.len();
        let k = if n > 10 {
            3
        } else if n > 5 {
            2
        } else { 1 };

        let unique: HashSet<_> = kmers_without_n(self.deref().iter().cloned(), k)
            .map(|value| value.kmer).collect();
        unique.len() as f64 / min(n - k + 1, 4_usize.pow(k as u32)) as f64
    }

    fn write_fasta<T: Write>(&self, writer: &mut T, name: &[u8]) -> Result<()> {
        let seq = self.deref();
        writer.write_all(b">")?;
        writer.write_all(name)?;
        writer.write_all(b"\n")?;
        for i in (0..seq.len()).step_by(80) {
            writer.write_all(&seq[i..min(i + 80, seq.len())])?;
            writer.write_all(b"\n")?;
        }
        Ok(())
    }

    fn to_str(&self) -> &str {
        std::str::from_utf8(&self).expect("Sequence does not satisfy UTF-8")
    }
}

impl Sequence for Text { }
impl<'a> Sequence for TextSlice<'a> { }

pub fn kmer_to_sequence(mut kmer: u32, len: usize) -> Text {
    let mut res = vec![0; len];
    for i in 0..len {
        res[len - i - 1] = int_to_nt(kmer % 4);
        kmer /= 4;
    }
    res
}

pub fn kmers<I: IntoIterator<Item = u8>>(sequence: I, k: usize) -> impl Iterator<Item = u32> {
    let k1 = k - 1;
    let cut_old = (1 << 2 * k1) - 1;

    let mut kmer = 0;
    sequence.into_iter().enumerate().filter_map(move |(i, nt)| {
        kmer = ((kmer & cut_old) << 2) + nt_to_int(nt);
        if i >= k1 {
            Some(kmer)
        } else {
            None
        }
    })
}

#[derive(Clone, Copy, PartialOrd, Ord, PartialEq, Eq)]
pub struct Kmer {
    pub kmer: u32,
    pub index: u32,
}

impl Kmer {
    pub fn kmer_first(a: &Kmer, b: &Kmer) -> Ordering {
        (a.kmer, a.index).cmp(&(b.kmer, b.index))
    }

    pub fn index_first(a: &Kmer, b: &Kmer) -> Ordering {
        (a.index, a.kmer).cmp(&(b.index, b.kmer))
    }
}

impl Debug for Kmer {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{}@{}", self.kmer, self.index)
    }
}

pub fn kmers_without_n<I: IntoIterator<Item = u8>>(sequence: I, k: usize)
        -> impl Iterator<Item = Kmer> {
    let k1 = k - 1;
    let cut_old = (1 << 2 * k1) - 1;

    let mut kmer = 0;
    let mut kmer_len = 0;
    sequence.into_iter().enumerate().filter_map(move |(i, nt)| {
        if nt == b'N' {
            kmer = 0;
            kmer_len = 0;
        } else {
            kmer = ((kmer & cut_old) << 2) + nt_to_int(nt);
            kmer_len += 1;
        }
        if kmer_len >= k {
            Some(Kmer {
                index: (i + 1 - k) as u32,
                kmer,
            })
        } else {
            None
        }
    })
}