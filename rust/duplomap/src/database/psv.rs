use std::cmp::{min, max};
use std::hash::{Hash, Hasher};
use std::io::Write;

use bio::utils::TextSlice;
use twox_hash::XxHash64;

use biology::spans::{Range, PositiveSpan, DirectionalSpan, SpanSequence, Span, SpanTools};
use biology::{Genome, Sequence};
use database::genotype::{self, Genotypes};
use alignment::hmm;
use utils::{Log, log_numbers, vcf};
use utils::common::{GzWriter};
use mapping::parameters::{Parameters, Priors};

pub fn find_variable_range(seq1: TextSlice, seq2: TextSlice) -> [Range; 2] {
    let min_len = min(seq1.len(), seq2.len()) as u32;
    // Start should represent the first non-equal index
    let mut start = min_len - 1;
    for i in 0..min_len - 1 {
        if seq1[i as usize] != seq2[i as usize] {
            start = i;
            break;
        }
    }

    // len - end represents the last non-equal index
    // len - end >= start => end <= min_len - start
    let mut end = min_len - start;
    for i in 1..min_len - start {
        if seq1[seq1.len() - i as usize] != seq2[seq2.len() - i as usize] {
            end = i;
            break;
        }
    }
    [Range::new(start, seq1.len() as u32 - end + 1), Range::new(start, seq2.len() as u32 - end + 1)]
}

#[derive(Clone, Copy)]
pub struct PsvHash(u64);

impl PsvHash {
    fn new(span1: &DirectionalSpan, span2: &DirectionalSpan) -> Self {
        let mut hasher = XxHash64::default();
        span1.hash(&mut hasher);
        span2.hash(&mut hasher);
        Self(hasher.finish() as u64)
    }

    fn to_string(&self) -> String {
        base64::encode_config(&self.0.to_le_bytes(), base64::URL_SAFE_NO_PAD)
    }
}

pub struct Psv {
    span_seqs: [SpanSequence<DirectionalSpan>; 2],
    variable_ranges: [Range; 2],
    genotypes: [Genotypes; 2],
    hash: PsvHash,
    p_reliable: Log,
    active: bool,
}

impl Psv {
    pub fn new(span1: DirectionalSpan, span2: DirectionalSpan, genome: &mut Genome) -> Self {
        let hash = PsvHash::new(&span1, &span2);
        let span1 = SpanSequence::new(span1, genome);
        let span2 = SpanSequence::new(span2, genome);
        assert!(span1.sequence() != span2.sequence(),
            "PSV cannot contain identical sequences:\n\t{}\n\t{}",
            span1.format(genome), span2.format(genome));

        let ranges = find_variable_range(span1.sequence(), span2.sequence());
        Psv {
            span_seqs: [span1, span2],
            variable_ranges: ranges,
            genotypes: [Genotypes::new(), Genotypes::new()],
            hash,
            p_reliable: Log::ONE,
            active: true,
        }
    }

    pub fn can_be_used(&self, params: &Parameters) -> bool {
        let len0 = self.span(0).len();
        let len1 = self.span(1).len();
        if len0 == len1 {
            self.complexity() >= params.psv_sub_complexity
        } else {
            let size_diff = max(len0, len1) - min(len0, len1);
            size_diff < params.psv_size_diff && self.complexity() >= params.psv_indel_complexity
        }
    }

    pub fn same_len(&self) -> bool {
        self.variable_ranges[0].len() == self.variable_ranges[1].len()
    }

    pub fn span_seq(&self, index: usize) -> &SpanSequence<DirectionalSpan> {
        &self.span_seqs[index]
    }

    pub fn span(&self, index: usize) -> &DirectionalSpan {
        self.span_seqs[index].span()
    }

    pub fn sequence(&self, index: usize) -> TextSlice {
        self.span_seqs[index].sequence()
    }

    pub fn var_sequence(&self, index: usize) -> TextSlice {
        &self.sequence(index)[self.variable_ranges[index].to_std_range()]
    }

    pub fn complexity(&self) -> f64 {
        (self.var_sequence(0).complexity() + self.var_sequence(1).complexity())
            .min(self.sequence(0).complexity() + self.sequence(1).complexity()) * 0.5
    }

    pub fn var_span(&self, index: usize) -> DirectionalSpan {
        self.span(index).extract_region(
            self.variable_ranges[index].start(), self.variable_ranges[index].end())
    }

    pub fn var_range(&self, index: usize) -> &Range {
        &self.variable_ranges[index]
    }

    pub fn corresponds_to(&self, span: &PositiveSpan) -> Option<usize> {
        if self.span(0).undirectional_eq(span) {
            Some(0)
        } else if self.span(1).undirectional_eq(span) {
            Some(1)
        } else {
            None
        }
    }

    pub fn genotype(&self, index: usize) -> &Genotypes {
        &self.genotypes[index]
    }

    pub fn genotype_mut(&mut self, index: usize) -> &mut Genotypes {
        &mut self.genotypes[index]
    }

    pub fn update_genotypes(&mut self, priors: &Priors, genome: &Genome, iteration: &str,
            psv_writer: &mut Option<GzWriter>) {
        if self.genotypes[0].tmp_read_depth() == 0 && self.genotypes[0].read_depth() == 0
                && self.genotypes[1].tmp_read_depth() == 0 && self.genotypes[1].read_depth() == 0 {
            return;
        }
        if let Some(ref mut writer) = psv_writer {
            write!(writer, "{}\t{}\t{}\t", iteration, self.hash(), if self.active { 'T' } else { 'F' })
                .expect("Failed to write PSVs");
            write!(writer, "{}:{}\t{}:{}\t",
                genome.chrom_name(self.span(0).chrom_id()), self.var_start(0) + 1,
                genome.chrom_name(self.span(1).chrom_id()), self.var_start(1) + 1).expect("Failed to write PSVs");
        }
        self.p_reliable = genotype::select_best_genotypes(&mut self.genotypes, priors, psv_writer)
            .expect("Failed to write PSVs");
    }

    pub fn align(&self, sequence: TextSlice, params: &hmm::Parameters) -> [Log; 2] {
        let mut probs = [hmm::align(sequence, self.sequence(0), params),
            hmm::align(sequence, self.sequence(1), params)];
        log_numbers::normalize(&mut probs);
        probs
    }

    pub fn hash(&self) -> String {
        self.hash.to_string()
    }

    // Returns sequence with anchors in lowercase, and the variable region in uppercase.
    pub fn camel_seq(&self, index: usize) -> Vec<u8> {
        let var_range = self.variable_ranges[index].to_std_range();
        let forward: Vec<_> = self.sequence(index).iter().enumerate()
            .map(|(i, nt)| if var_range.contains(&i) { *nt } else { nt.to_ascii_lowercase() })
            .collect();
        if self.span(index).strand() {
            forward
        } else {
            forward.reverse_complement()
        }
    }

    pub fn use_for_loc_probs(&self) -> bool {
        self.active && self.genotypes[0].use_for_loc_probs() && self.genotypes[1].use_for_loc_probs()
    }

    pub fn use_for_conflicts(&self) -> bool {
        self.active && self.p_reliable.real() >= 0.99
            && self.genotypes[0].use_for_conflicts()
            && self.genotypes[1].use_for_conflicts()
    }

    pub fn active(&self) -> bool {
        self.active
    }

    pub fn p_reliable(&self) -> Log {
        self.p_reliable
    }

    pub fn var_start(&self, index: usize) -> u32 {
        let span = self.span(index);
        if span.strand() {
            span.start() + self.variable_ranges[index].start()
        } else {
            span.start() + span.len() - self.variable_ranges[index].end()
        }
    }

    pub fn to_vcf(&self, record: &mut vcf::Record, index: usize, genome: &Genome, db_name: &str) {
        let span = self.span(index);
        record.set_chrom_id(span.chrom_id());
        record.set_pos(self.var_start(index) + 1);

        let ref_seq = &self.sequence(index)[self.variable_ranges[index].to_std_range()];
        let alt_seq = &self.sequence(1 - index)[self.variable_ranges[1 - index].to_std_range()];
        if span.strand() {
            record.update_ref(ref_seq.to_str());
            record.update_alt(alt_seq.to_str());
        } else {
            record.update_ref(ref_seq.reverse_complement().to_str());
            record.update_alt(alt_seq.reverse_complement().to_str());
        }

        record.push_info("db", &db_name);
        record.push_info("psv", &self.hash());

        let paralog_span = self.span(1 - index);
        let paralog_chrom = genome.chrom_name(paralog_span.chrom_id());
        let paralog = format!("{}:{}:{}", paralog_chrom, self.var_start(1 - index) + 1,
            if paralog_span.strand() == span.strand() { '+' } else { '-' });
        record.push_info("paralog", &paralog);
    }

    pub(crate) fn check_ambiguous_reads(&mut self, max_rate: f32) -> bool {
        if self.genotypes[0].ambiguous_reads() >= max_rate || self.genotypes[1].ambiguous_reads() >= max_rate {
            trace!("    PSV {} has too many ambiguously aligned reads", self.hash());
            self.active = false;
        }
        self.active
    }
}
