use std::sync::{Mutex, Arc, TryLockError};
use std::sync::atomic::{
    AtomicBool,
    AtomicU32,
    Ordering::Relaxed,
};
use std::collections::VecDeque;
use std::thread::{self, JoinHandle};
use std::time;

// Have to define FnBox as std::boxed::FnBox is unstable, and Box<dyn FnOnce> does not work yet
pub trait FnBox<W> {
    fn call_box(self: Box<Self>, worker: &mut W);
}

impl<W, F: FnOnce(&mut W)> FnBox<W> for F {
    fn call_box(self: Box<F>, worker: &mut W) {
        (*self)(worker)
    }
}

pub type Job<W> = Box<dyn FnBox<W> + Send>;

pub trait Execute: Sized {
    fn execute(&mut self, function: Job<Self>) {
        function.call_box(self);
    }
}

fn worker_routine<W: Execute>(worker_mutex: Arc<Mutex<W>>, jobs: Arc<Mutex<VecDeque<Job<W>>>>,
        is_finished: Arc<AtomicBool>, finished_jobs: Arc<AtomicU32>) {
    let sleep_duration = time::Duration::from_millis(10);
    loop {
        let job = jobs.lock().expect("Stop due to panic in another thread")
            .pop_front();
        match job {
            Some(function) => {
                match worker_mutex.try_lock() {
                    Ok(mut worker) => worker.execute(function),
                    Err(TryLockError::WouldBlock) =>
                        panic!("Worker is modified while job queue is non-empty"),
                    Err(TryLockError::Poisoned(_)) => panic!("Stop due to panic in another thread"),
                };
                finished_jobs.fetch_add(1, Relaxed);
            },
            None => {
                if is_finished.load(Relaxed) {
                    return;
                } else {
                    thread::sleep(sleep_duration);
                }
            },
        }
    }
}

pub struct ThreadPool<W: Execute + Send + 'static> {
    handles: Vec<JoinHandle<()>>,
    jobs: Arc<Mutex<VecDeque<Job<W>>>>,
    workers: Vec<Arc<Mutex<W>>>,
    is_finished: Arc<AtomicBool>,
    finished_jobs: Arc<AtomicU32>,
}

impl<W: Execute + Send + 'static> ThreadPool<W> {
    pub fn new<I: Iterator<Item = W>>(workers: I) -> Self {
        let jobs = Arc::new(Mutex::new(VecDeque::new()));
        let is_finished = Arc::new(AtomicBool::new(false));
        let finished_jobs = Arc::new(AtomicU32::new(0));
        let workers: Vec<_> = workers.map(|worker| Arc::new(Mutex::new(worker))).collect();

        let handles = workers.iter().enumerate().map(|(i, worker)| {
            let worker = Arc::clone(worker);
            let jobs = Arc::clone(&jobs);
            let is_finished = Arc::clone(&is_finished);
            let finished_jobs = Arc::clone(&finished_jobs);
            thread::Builder::new().name(format!("worker{}", i + 1))
                .spawn(move || worker_routine(worker, jobs, is_finished, finished_jobs))
                .expect("Failed to create a worker thread")
        }).collect();

        Self {
            handles, jobs, workers, is_finished, finished_jobs,
        }
    }

    pub fn execute<F: FnBox<W> + Send + 'static>(&mut self, job: F) {
        self.jobs.lock().expect("Stop due to panic in another thread").push_back(Box::new(job));
    }

    pub fn join(self) {
        self.is_finished.store(true, Relaxed);
        self.handles.into_iter()
            .for_each(|handle| handle.join().expect("Failed to wait on a thread"));
    }

    pub fn finished_jobs(&self) -> Arc<AtomicU32> {
        Arc::clone(&self.finished_jobs)
    }

    pub fn reset_finished_jobs(&self) {
        self.finished_jobs.store(0, Relaxed);
    }

    pub fn finished_at_least(&self, n_jobs: u32) -> bool {
        self.finished_jobs.load(Relaxed) >= n_jobs
    }

    pub fn for_each<F: Fn(&mut W)>(&self, f: F) {
        for worker_mutex in self.workers.iter() {
            let mut worker = worker_mutex.lock().expect("Stop due to panic in another thread");
            f(&mut worker);
        }
    }
}
