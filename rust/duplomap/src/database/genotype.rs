use std::fmt::{Display, Formatter};
use std::cmp::{min, max};
use std::io::{self, Write};

use utils::log_numbers::{self, Log};
use database::heavy_database::PsvId;
use mapping::parameters::Priors;
use utils::vcf;
use utils::common::GzWriter;

const PLOIDITY: usize = 2;
const N_GT: usize = PLOIDITY + 1;
const LOG_PLOIDITY: Log = Log::TWO;
const N_SUPP: usize = 3;

pub struct GenotypeUpdate {
    psv_id: PsvId,
    genotype_index: bool,
    prob1: Log,
    prob2: Log,
}

impl GenotypeUpdate {
    pub fn new(psv_id: PsvId, genotype_index: usize, prob1: Log, prob2: Log) -> GenotypeUpdate {
        GenotypeUpdate {
            genotype_index: genotype_index != 0,
            psv_id, prob1, prob2,
        }
    }

    pub fn psv_id(&self) -> PsvId {
        self.psv_id
    }

    pub fn genotype_index(&self) -> usize {
        self.genotype_index as usize
    }

    pub fn prob1(&self) -> Log {
        self.prob1
    }

    pub fn prob2(&self) -> Log {
        self.prob2
    }
}

fn corresponding_probability(count_0: usize, allele_0: Log, allele_1: Log) -> Log {
    allele_0 * Log::from_real((PLOIDITY - count_0) as f64)
        + allele_1 * Log::from_real(count_0 as f64)
}

#[derive(Clone)]
pub(crate) struct Information {
    probs: [Log; N_GT],
    read_depth: u32,
    // How many reads support 0, 1 or neither.
    support: [i32; N_SUPP],
}

impl Information {
    fn new() -> Information {
        Information {
            probs: [Log::ONE; N_GT],
            read_depth: 0,
            support: [0; N_SUPP],
        }
    }

    fn clear(&mut self) {
        self.probs.iter_mut().for_each(|prob| *prob = Log::ONE);
        self.read_depth = 0;
        self.support.iter_mut().for_each(|x| *x = 0);
    }

    fn apply_update(&mut self, update: &GenotypeUpdate, ambiguous_fold_diff: f32) {
        for (i, prob) in self.probs.iter_mut().enumerate() {
            *prob *= corresponding_probability(i, update.prob1(), update.prob2()) / LOG_PLOIDITY;
        }

        self.read_depth += 1;
        let prob_ratio = (max(update.prob1(), update.prob2()) / min(update.prob1(), update.prob2())).real() as f32;
        if prob_ratio >= ambiguous_fold_diff && update.prob1() > update.prob2() {
            self.support[0] += 1;
        } else if prob_ratio >= ambiguous_fold_diff && update.prob2() > update.prob1() {
            self.support[1] += 1;
        } else {
            self.support[2] += 1;
        }
    }

    fn ambiguous_reads(&self) -> f32 {
        self.support[2] as f32 / self.read_depth as f32
    }
}

impl Display for Information {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{:?}.  {} reads ({} ref, {} alt, {} neither)",
            self.probs, self.read_depth, self.support[0], self.support[1], self.support[2])
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum GenotypeValue {
    HomRef = 0,
    Hetero = 1,
    HomAlt = 2,
    Unknown = 255,
}

impl From<u8> for GenotypeValue {
    fn from(count_1: u8) -> GenotypeValue {
        match count_1 {
            0 => GenotypeValue::HomRef,
            1 => GenotypeValue::Hetero,
            2 => GenotypeValue::HomAlt,
            _ => panic!("Unexpected genotype index: {}", count_1),
        }
    }
}

impl std::fmt::Display for GenotypeValue {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        match self {
            GenotypeValue::HomRef => write!(f, "0/0"),
            GenotypeValue::Hetero => write!(f, "0/1"),
            GenotypeValue::HomAlt => write!(f, "1/1"),
            GenotypeValue::Unknown => write!(f, "."),
        }
    }
}

impl serde::Serialize for GenotypeValue {
    fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        serializer.collect_str(&self)
    }
}

pub struct Genotypes {
    saved_info: Information,
    tmp_info: Information,
    best_genotype: GenotypeValue,
    genotype_quality: f64,
}

impl Genotypes {
    pub fn new() -> Genotypes {
        Genotypes {
            saved_info: Information::new(),
            tmp_info: Information::new(),
            best_genotype: GenotypeValue::HomRef,
            genotype_quality: 0.0,
        }
    }

    pub fn apply_update(&mut self, update: &GenotypeUpdate, ambiguous_fold_diff: f32) {
        self.tmp_info.apply_update(update, ambiguous_fold_diff);
    }

    pub(crate) fn tmp_read_depth(&self) -> u32 {
        self.tmp_info.read_depth
    }

    fn save_tmp(&mut self) {
        self.saved_info.clone_from(&self.tmp_info);
        self.tmp_info.clear();
        log_numbers::normalize(&mut self.saved_info.probs);
    }

    pub fn read_depth(&self) -> u32 {
        self.saved_info.read_depth
    }

    pub(crate) fn ambiguous_reads(&self) -> f32 {
        self.saved_info.ambiguous_reads()
    }

    pub fn best_genotype(&self) -> GenotypeValue {
        self.best_genotype
    }

    pub fn genotype_quality(&self) -> f64 {
        self.genotype_quality
    }

    pub fn update_vcf_record(&self, record: &mut vcf::Record, active: bool) {
        record.push_format("GT", &[&self.best_genotype]);

        let mut pass = true;
        if !active {
            pass = false;
            record.push_filter("ambiguous");
        }
        if pass {
            record.push_filter("PASS");
        }
        let genotype_quality = self.genotype_quality.min(std::i32::MAX as f64) as i32;
        record.push_format("GQ", &[&genotype_quality]);
        record.set_qual(self.genotype_quality.min(std::i32::MAX as f64) as f32);

        record.push_format("DP", &[&self.saved_info.read_depth]);
        let gt_probs: Vec<_> = self.saved_info.probs.iter()
            .map(|prob| format!("{:.2}", prob.log10())).collect();
        let gt_probs = gt_probs.join(",");
        record.push_format("GL", &[&gt_probs]);
        let support_str = format!("{},{},{}", self.saved_info.support[0],
            self.saved_info.support[1], self.saved_info.support[2]);
        record.push_format("sp", &[&support_str]);
    }

    pub fn use_for_loc_probs(&self) -> bool {
        self.best_genotype != GenotypeValue::Hetero
    }

    pub fn use_for_conflicts(&self) -> bool {
        self.best_genotype != GenotypeValue::Hetero
    }
}

formatted!(Precision3, "{:.3}");

fn write_join<W: Write, T: Display, I: Iterator<Item = T>>(writer: &mut W, iter: I) -> io::Result<()> {
    for (i, el) in iter.enumerate() {
        if i == 0 {
            write!(writer, "{}", el)?;
        } else {
            write!(writer, ",{}", el)?;
        }
    }
    write!(writer, "\t")?;
    Ok(())
}

// Returns P(PSV is reliable).
pub(crate) fn select_best_genotypes(genotypes: &mut [Genotypes; PLOIDITY], priors: &Priors,
        psv_writer: &mut Option<GzWriter>) -> io::Result<Log> {
    genotypes[0].save_tmp();
    genotypes[1].save_tmp();
    if let Some(ref mut writer) = psv_writer {
        write_join(writer, genotypes[0].saved_info.probs.iter().map(|p| Precision3(p.log10())))?;
        write_join(writer, genotypes[1].saved_info.probs.iter().map(|p| Precision3(p.log10())))?;
        write_join(writer, genotypes[0].saved_info.support.iter())?;
        write_join(writer, genotypes[1].saved_info.support.iter())?;
    }

    let mut probs = [Log::ZERO; N_GT * N_GT];
    let mut best_prob = Log::ZERO;
    let mut best_i = 0;
    let mut best_j = 0;

    for i in 0..N_GT {
        for j in 0..N_GT {
            let prob = genotypes[0].saved_info.probs[i] * priors.values[i]
                    * genotypes[1].saved_info.probs[j] * priors.values[j];
            if prob > best_prob {
                best_prob = prob;
                best_i = i;
                best_j = j;
            }
            probs[i * N_GT + j] = prob;
        }
    }
    log_numbers::normalize(&mut probs);
    trace!("        Prob matrix: {:?}", probs);

    for i in 0..N_GT {
        genotypes[0].saved_info.probs[i] = Log::ZERO;
        genotypes[1].saved_info.probs[i] = Log::ZERO;
        for j in 0..N_GT {
            // i, j
            genotypes[0].saved_info.probs[i] += probs[i * N_GT + j];
            // j, i
            genotypes[1].saved_info.probs[i] += probs[j * N_GT + i];
        }
    }
    log_numbers::normalize(&mut genotypes[0].saved_info.probs);
    log_numbers::normalize(&mut genotypes[1].saved_info.probs);

    genotypes[0].best_genotype = GenotypeValue::from(best_i as u8);
    genotypes[1].best_genotype = GenotypeValue::from(best_j as u8);
    genotypes[0].genotype_quality = log_numbers::phred_score(&genotypes[0].saved_info.probs);
    genotypes[1].genotype_quality = log_numbers::phred_score(&genotypes[1].saved_info.probs);

    if let Some(ref mut writer) = psv_writer {
        write_join(writer, genotypes[0].saved_info.probs.iter().map(|p| Precision3(p.log10())))?;
        write_join(writer, genotypes[1].saved_info.probs.iter().map(|p| Precision3(p.log10())))?;
        writeln!(writer, "{}\t{:.1}\t{}\t{:.1}\t{:.1}", genotypes[0].best_genotype, genotypes[0].genotype_quality,
            genotypes[1].best_genotype, genotypes[1].genotype_quality, 100.0 * probs[0].real())?;
    }
    Ok(probs[0])
}
