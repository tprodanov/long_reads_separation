#[macro_use] extern crate slog_scope;
#[macro_use] extern crate clap;
extern crate pbr;
extern crate flate2;

extern crate duplomap;

use std::fs::File;
use std::path::Path;
use std::io::{self, stderr, Write, BufWriter};
use std::cmp::min;

use clap::App;
use pbr::ProgressBar;
use flate2::write::GzEncoder;
use flate2::Compression;

use duplomap::biology::{self, Sequence};
use duplomap::database;
use duplomap::utils::common::{self, Matches};
use duplomap::utils::vcf::{self, VcfWrite};
use duplomap::biology::spans::Span;

fn parse_parameters(matches: &impl Matches) -> database::Parameters {
    let high_copy_num = matches.get_vec_or_default("skip-copy-num", vec![6_u32, 3000]);
    database::Parameters {
        anchor_size: matches.get_value("anchor"),
        area_radius: matches.get_value("area"),
        window_size: matches.get_value("window-size"),
        min_windows: matches.get_value("min-windows"),
        window_similarity: matches.get_value("window-similarity"),

        exclude_seq_simil: matches.get_value("seq-similarity"),
        exclude_length: matches.get_value("length"),
        components_distance: matches.get_value("comp-dist"),

        high_copy_num: min(high_copy_num[0], 255) as u8,
        high_copy_num_length: high_copy_num[1],

        minimap_kmer_size: matches.get_value("minimap-kmer"),
    }
}

fn write_header(writer: &mut impl VcfWrite, genome: &biology::Genome) -> io::Result<()> {
    writer.write_line("##fileformat=VCFv4.2")?;
    for i in 0..genome.count_chromosomes() as u32 {
        writer.add_chromosome(genome.chrom_name(i).to_string(), genome.chrom_len(i))?;
    }

    writer.write_line("##INFO=<ID=db,Number=1,Type=String,Description=\"Database Name\">")?;
    writer.write_line("##INFO=<ID=psv,Number=1,Type=String,Description=\"PSV Hash\">")?;
    writer.write_line("##INFO=<ID=paralog,Number=1,Type=String,\
        Description=\"Paralogous Position\">")?;
    writer.write_line("##INFO=<ID=complexity,Number=1,Type=Float,Description=\"PSV complexity\">")?;
    writer.write_line("##INFO=<ID=seq,Number=1,Type=String,Description=\"Full sequence, \
        including anchors\">")?;
    writer.write_samples(&[])
}

fn update_vcf(db_name: &str, database: &database::Database, genome: &mut biology::Genome,
        records: &mut Vec<vcf::Record>) {
    for [span1, span2] in database.psvs.iter() {
        let psv = database::psv::Psv::new(span1.clone(), span2.clone(), genome);
        for i in 0..2 {
            let mut record = vcf::Record::new();
            psv.to_vcf(&mut record, i, genome, db_name);
            record.push_info("complexity", &format!("{:.3}", psv.complexity()));
            record.push_info("seq", &psv.camel_seq(i).to_str());
            record.set_qual(100.0);
            records.push(record);
        }
    }
}

fn main() {
    let timer = std::time::Instant::now();
    let yaml = load_yaml!("prepare.yml");
    let authors = env!("CARGO_PKG_AUTHORS").replace(':', ", ");
    let parser = App::from_yaml(yaml)
        .version(&crate_version!()[..])
        .author(&authors as &str);
    let matches = common::matches_to_map(&parser.get_matches());
    match common::create_main_directory(&matches) {
        Err(e) => panic!("{}", e),
        _ => { },
    }

    let _guard = common::create_logger(&timer, &matches, "prepare.log");
    info!("Version {}", &crate_version!());
    let mut genome = biology::Genome::from_file(matches.get_str("reference"));
    let params = parse_parameters(&matches);
    let output = Path::new(matches.get_str("output"));

    let annotation = matches.get("annotate").map(|path| {
        info!("Loading gene annotation");
        database::annotate::load_gff(&path[0], &genome).unwrap_or_else(|e| panic!("{}", e))
    });

    info!("Loading duplications");
    let inp = matches.get_str("input");
    let mut dot_file = File::create(output.join("duplications.dot"))
        .unwrap_or_else(|_| panic!("Could not create 'duplications.dot'"));

    let duplications = database::duplications::load(inp, &genome);
    let duplications = database::duplications::filter(duplications, &genome, &params);
    let high_cn_regions = database::duplications::find_high_copy_regions(&duplications,
        params.high_copy_num, params.high_copy_num_length);
    let mut high_cn_set = biology::SpanSet::new(genome.count_chromosomes());
    let mut high_cn_bed = File::create(output.join("high_copy_num.bed"))
        .expect("Could not create 'high_cn_regions.bed' file");
    for region in &high_cn_regions {
        high_cn_set.insert(region, ());
        writeln!(high_cn_bed, "{}\t{}\t{}", genome.chrom_name(region.chrom_id()), region.start(), region.end())
            .expect("Failed to write BED record");
    }
    std::mem::drop(high_cn_bed);

    info!("Aligning duplications");
    std::thread::sleep(std::time::Duration::from_millis(10));
    let mut pbar = ProgressBar::on(stderr(), duplications.len() as u64);
    let dup_alns = database::align_dup::align_all(&duplications, &mut genome, &params, &high_cn_set, &mut pbar);
    pbar.finish();

    let components = database::duplications::create_components(&dup_alns, &genome, &params, &mut dot_file);
    info!("Saved duplication graph to 'duplications.dot'");

    let mut summary = BufWriter::new(File::create(output.join("summary.csv"))
        .expect("Could not create a summary file"));
    writeln!(summary, "component\tdupl_regions\tsum_len\tmean_len\tcopy_num\tpsvs")
        .expect("Could not write summary");

    let mut description = BufWriter::new(File::create(output.join("description.txt"))
        .expect("Could not create a description file"));
    writeln!(description, "{}", std::env::args().collect::<Vec<_>>().join(" "))
        .expect("Could not write description");

    let vcf_filename = output.join("psvs.vcf.gz");
    let mut vcf_writer = vcf::Writer::create_compressed(&vcf_filename)
        .unwrap_or_else(|_| panic!("Could not open VCF file {}", vcf_filename.display()));
    write_header(&mut vcf_writer, &genome).expect("Could not write to the VCF file");
    let mut vcf_records = Vec::new();
    let mut bed_writer = File::create(output.join("regions.bed")).expect("Could not create 'regions.bed' file");
    let mut bed_records = Vec::new();

    info!("Extracting PSVs and saving components");
    std::thread::sleep(std::time::Duration::from_millis(10));
    let mut pbar = ProgressBar::on(stderr(), components.len() as u64);
    let mut db_names = Vec::new();
    for (i, component) in components.iter().enumerate() {
        pbar.tick();
        debug!("Analyzing component {}", i + 1);
        let mut db = database::construct::create_database(component.iter()
            .map(|i| &dup_alns[*i as usize]), &params, &mut genome);
        db.sort_and_merge();
        if db.duplicated_regions.is_empty() {
            warn!("Component {} has 0 aligned duplicated regions", i + 1);
            db_names.push(String::new());
            continue;
        }
        db.set_high_cn_regions(&high_cn_set);

        let genes = if let Some(ref value) = annotation {
            database::annotate::find_intersecting_genes(&db, &value)
        } else {
            Vec::with_capacity(0)
        };
        let name = database::annotate::select_name(&genes);
        let name = if !name.is_empty() {
            format!("{}-{}", i + 1, name)
        } else {
            (i + 1).to_string()
        };
        debug!("    Writing output to {}.db", name);

        let mut outp = File::create(output.join(format!("{}.db", name)))
            .unwrap_or_else(|_| panic!("Could not create output file \"{}\"", name));

        write!(summary, "{}\t", name).expect("Could not write summary");
        writeln!(description, "\nComponent {}\n{}", name, "=".repeat(name.len() + 10))
            .expect("Could not write description");
        database::annotate::write_duplications(&db, &mut description, &mut summary)
            .unwrap_or_else(|_| error!("Error while writing summary"));
        database::annotate::write_gene_annotation(&mut description, &genes, &genome)
            .unwrap_or_else(|_| error!("Failed to write gene annotation"));
        if matches.contains("human") {
            db.dump(&mut outp, &genome).unwrap_or_else(|e| panic!("{}", e));
        } else {
            db.dump_binary(BufWriter::new(GzEncoder::new(outp, Compression::default())), &genome)
                .unwrap_or_else(|e| panic!("{}", e));
        }
        update_vcf(&name, &db, &mut genome, &mut vcf_records);
        for region in db.duplicated_regions.iter() {
            bed_records.push((region.clone(), i))
        }
        db_names.push(name);
        pbar.inc();
    }

    vcf_records.sort_by(|a, b| a.cmp(b));
    for record in vcf_records {
        vcf_writer.write(&record).unwrap_or_else(|_| panic!("Cannot write vcf record"));
    }

    bed_records.sort_by(|a, b| a.0.start_comparator().cmp(&b.0.start_comparator()));
    for (region, db_ind) in bed_records {
        writeln!(bed_writer, "{}\t{}\t{}\t{}", genome.chrom_name(region.chrom_id()),
            region.start(), region.end(), db_names[db_ind]).expect("Failed to write BED record");
    }
    common::goodbye(&timer.elapsed());
}
