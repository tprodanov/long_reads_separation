// original author: Ben Pullman
// modified: Peter Edge, September 2017
// modified: Timofey Prodanov, April 2019

use std::cmp::{min, max};

use bio::utils::TextSlice;
use bam::record::cigar::Class;

use utils::Log;

// these parameters describe state transition probabilities for a pair HMM
// there are two kinds: "eq" transition probs and "neq" transition_probs
// the correct kind to use depends on sequence context.
// the "eq" probabilities are for the case where accepting a match results in equal bases
// the "neq" probabilities are for the case where accepting a match results in different bases

#[derive(Clone)]
pub struct TransitionProbs {
    pub match_from_match: Log,
    pub insertion_from_match: Log,
    pub deletion_from_match: Log,
    pub insertion_from_insertion: Log,
    pub match_from_insertion: Log,
    pub deletion_from_deletion: Log,
    pub match_from_deletion: Log,
}

impl Default for TransitionProbs {
    fn default() -> Self {
        TransitionProbs {
            match_from_match: Log::from_real(0.91229),
            insertion_from_match: Log::from_real(0.05735),
            deletion_from_match: Log::from_real(0.03036),
            match_from_deletion: Log::from_real(0.84287),
            deletion_from_deletion: Log::from_real(0.15713),
            match_from_insertion: Log::from_real(0.73059),
            insertion_from_insertion: Log::from_real(0.26941),
        }
    }
}

impl TransitionProbs {
    pub fn debug_print(&self) {
        debug!("Transition probabilities:");
        debug!("    match     -> match:      {:.5}", self.match_from_match.real());
        debug!("    match     -> insertion:  {:.5}", self.insertion_from_match.real());
        debug!("    match     -> deletion:   {:.5}", self.deletion_from_match.real());
        debug!("    insertion -> insertion:  {:.5}", self.insertion_from_insertion.real());
        debug!("    insertion -> match:      {:.5}", self.match_from_insertion.real());
        debug!("    deletion  -> deletion:   {:.5}", self.deletion_from_deletion.real());
        debug!("    deletion  -> match:      {:.5}", self.match_from_deletion.real());
    }

    pub fn transition_prob(&self, prev: Class, curr: Class) -> Log {
        use bam::record::cigar::Class::*;
        match (prev, curr) {
            (Match, Match) => self.match_from_match,
            (Match, Insertion) => self.insertion_from_match,
            (Match, Deletion) => self.deletion_from_match,

            (Insertion, Match) => self.match_from_insertion,
            (Insertion, Insertion) => self.insertion_from_insertion,
            (Insertion, Deletion) => self.match_from_insertion * self.deletion_from_match,

            (Deletion, Match) => self.match_from_deletion,
            (Deletion, Deletion) => self.deletion_from_deletion,
            (Deletion, Insertion) => self.match_from_deletion * self.insertion_from_match,

            (Hard, _) | (_, Hard) =>
                panic!("Cannot calculate transition probability for hard clipping!"),
        }
    }
}

#[derive(Clone)]
pub struct EmissionProbs {
    pub equal: Log,
    pub not_equal: Log,
    pub insertion: Log,
    pub deletion: Log,
}

impl Default for EmissionProbs {
    fn default() -> Self {
        EmissionProbs {
            equal: Log::from_real(0.9733),
            not_equal: Log::from_real(0.0089),
            insertion: Log::from_real(1.0),
            deletion: Log::from_real(1.0),
        }
    }
}

impl EmissionProbs {
    pub fn debug_print(&self) {
        debug!("Emission probabilities:");
        debug!("    equal:      {:.5}", self.equal.real());
        debug!("    not equal:  {:.5}", self.not_equal.real());
        debug!("    insertion:  {:.5}", self.insertion.real());
        debug!("    deletion:   {:.5}", self.deletion.real());
    }
}

#[derive(Clone)]
pub struct Parameters {
    pub transition_probs: TransitionProbs,
    pub emission_probs: EmissionProbs,
    pub min_band_width: u32,
}

impl Default for Parameters {
    fn default() -> Self {
        Parameters {
            transition_probs: TransitionProbs::default(),
            emission_probs: EmissionProbs::default(),
            min_band_width: Parameters::default_band_width(),
        }
    }
}

impl Parameters {
    pub fn debug_print(&self) {
        self.transition_probs.debug_print();
        self.emission_probs.debug_print();
        debug!("Min band width: {}", self.min_band_width);
    }

    pub fn default_band_width() -> u32 {
        20
    }
}

pub fn align(v: TextSlice, w: TextSlice, params: &Parameters) -> Log {
    if v.is_empty() || w.is_empty() {
        return Log::ZERO;
    }
    let len_diff = max(v.len(), w.len()) - min(v.len(), w.len());
    let band_width = params.min_band_width as usize + len_diff;

    let mut lower_prev = vec![Log::ZERO; w.len() + 1];
    let mut middle_prev = vec![Log::ZERO; w.len() + 1];
    let mut upper_prev = vec![Log::ZERO; w.len() + 1];
    let mut lower_curr = vec![Log::ZERO; w.len() + 1];
    let mut middle_curr = vec![Log::ZERO; w.len() + 1];
    let mut upper_curr = vec![Log::ZERO; w.len() + 1];

    middle_prev[0] = Log::ONE;

    upper_prev[1] = params.transition_probs.deletion_from_match;
    for j in 2..(w.len() + 1) {
        upper_prev[j] = upper_prev[j - 1] * params.transition_probs.deletion_from_deletion;
        middle_prev[j] = Log::ZERO;
    }

    let t = &params.transition_probs;
    let e = &params.emission_probs;
    for i in 1..v.len() + 1 {
        let band_middle = (w.len() * i) / v.len();
        let band_start = if band_middle >= band_width / 2 + 1 {
            band_middle - band_width / 2
        } else {
            1
        };
        let band_end = if band_middle + band_width / 2 <= w.len() {
            band_middle + band_width / 2
        } else {
            w.len()
        };

        if band_start == 1 {
            upper_curr[0] = Log::ZERO;
            middle_curr[0] = Log::ZERO;
            if i == 1 {
                lower_curr[0] = t.insertion_from_match
            } else {
                lower_curr[0] = lower_prev[0] * t.insertion_from_insertion;
            }
        }

        for j in band_start..band_end + 1 {
            let lower_continue = lower_prev[j] * t.insertion_from_insertion;
            let lower_from_middle = middle_prev[j] * t.insertion_from_match;
            lower_curr[j] = e.insertion * (lower_continue + lower_from_middle);

            let upper_continue = upper_curr[j - 1] * t.deletion_from_deletion;
            let upper_from_middle = middle_curr[j - 1] * t.deletion_from_match;
            upper_curr[j] = e.deletion * (upper_continue + upper_from_middle);

            let middle_from_lower = lower_prev[j - 1] * t.match_from_insertion;
            let middle_continue = middle_prev[j - 1] * t.match_from_match;
            let middle_from_upper = upper_prev[j - 1] * t.match_from_deletion;
            let match_emission = if v[i - 1] == w[j - 1] { e.equal } else { e.not_equal };
            middle_curr[j] = match_emission
                * (middle_from_lower + middle_continue + middle_from_upper);
        }

        for j in band_start - 1..band_end + 1 {
            upper_prev[j] = upper_curr[j];
            middle_prev[j] = middle_curr[j];
            lower_prev[j] = lower_curr[j];
        }

        upper_curr[band_start] = Log::ZERO;
        middle_curr[band_start] = Log::ZERO;
        lower_curr[band_start] = Log::ZERO;
    }

    middle_prev[w.len()]
}
