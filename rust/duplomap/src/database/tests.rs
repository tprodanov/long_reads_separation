use database::psv;
use biology::spans::{Range};

// PSVs

#[test]
fn variable_range_simple() {
    //           0123456789
    let seq1 = b"ACGTACTGTG".to_vec();
    let seq2 = b"ACGTCGTGTG".to_vec();
    let ranges = psv::find_variable_range(&seq1, &seq2);
    assert_eq!(ranges[0], Range::new(4, 6));
    assert_eq!(ranges[1], Range::new(4, 6));

    let seq1 = b"ACGTATGTG".to_vec();
    let seq2 = b"ACGTCGTGTG".to_vec();
    let ranges = psv::find_variable_range(&seq1, &seq2);
    assert_eq!(ranges[0], Range::new(4, 5));
    assert_eq!(ranges[1], Range::new(4, 6));

    let seq1 = b"ACGTAGTG".to_vec();
    let seq2 = b"ACGTCGAGTG".to_vec();
    let ranges = psv::find_variable_range(&seq1, &seq2);
    assert_eq!(ranges[0], Range::new(4, 5));
    assert_eq!(ranges[1], Range::new(4, 7));
}

#[test]
fn variable_range_homop() {
    let seq1 = b"ACGTAAAAAAACGT".to_vec();
    let seq2 = b"ACGTAAAAAAAACGT".to_vec();
    let ranges = psv::find_variable_range(&seq1, &seq2);
    assert_eq!(ranges[0], Range::new(3, 12));
    assert_eq!(ranges[1], Range::new(3, 13));
}
