use std::f64;
use std::cmp::Ordering;
use std::ops;
use std::fmt;
use std::iter;

#[derive(Copy, Clone)]
pub struct Log(f64);

const LN_2: f64  = 0.6931471805599453;
const LN_10: f64 = 2.3025850929940457;

impl Log {
    pub const ZERO: Log = Log(f64::NEG_INFINITY);
    pub const ONE: Log = Log(0.0);
    pub const TWO: Log = Log(LN_2);

    pub fn from_real(real_value: f64) -> Log {
        assert!(real_value >= 0.0, "Cannot get log from a negative value: {}", real_value);

        if real_value == 0.0 {
            Log::ZERO
        } else {
            Log(real_value.ln())
        }
    }

    pub fn from_log10(log_value: f64) -> Log {
        assert!(!log_value.is_nan(), "Cannot construct log from NaN");
        Log(log_value * LN_10)
    }

    pub fn log10(&self) -> f64 {
        self.0 / LN_10
    }

    pub fn ln(&self) -> f64 {
        self.0
    }

    pub fn is_zero(&self) -> bool {
        self.0 == f64::NEG_INFINITY
    }

    pub fn real(&self) -> f64 {
        self.0.exp()
    }

    pub fn pow<T: Into<f64>>(&self, power: T) -> Log {
        Log(self.0 * power.into())
    }

    pub fn inverse(&self) -> Log {
        Log(-self.0)
    }
}

impl PartialEq for Log {
    fn eq(&self, other: &Log) -> bool {
        self.0 == other.0
    }
}

impl Eq for Log {}

// Can implement as there can be no NaN
impl Ord for Log {
    fn cmp(&self, other: &Log) -> Ordering {
        if self.0 < other.0 {
            Ordering::Less
        } else if self.0 == other.0 {
            Ordering::Equal
        } else {
            Ordering::Greater
        }
    }
}

impl PartialOrd for Log {
    fn partial_cmp(&self, other: &Log) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl fmt::Display for Log {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "log({:.3}) = {:6.3}", self.real(), self.log10())
    }
}

impl fmt::Debug for Log {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:.3}", self.log10())
    }
}

impl Default for Log {
    fn default() -> Log {
        Log::ZERO
    }
}

impl_op!(* |a: Log, b: Log| -> Log {
    Log(a.0 + b.0)
});

impl_op!(*= |a: &mut Log, b: Log| {
    a.0 += b.0
});

impl_op!(/ |a: Log, b: Log| -> Log {
    assert!(!b.is_zero(), "Division by zero");
    Log(a.0 - b.0)
});

impl_op!(/= |a: &mut Log, b: Log| {
    assert!(!b.is_zero(), "Division by zero");
    a.0 -= b.0
});

const APPROXIMATION_THRESHOLD: f64 = 0.005;

// Approximate ln(1 + b / a), where alpha=ln(a), beta=ln(b), a > b
fn ln_one_plus_div(alpha: f64, beta: f64) -> f64 {
    let x = (beta - alpha).exp();
    if x < APPROXIMATION_THRESHOLD {
        x
    } else {
        (1.0 + x).ln()
    }
}

// Approximate ln(a + b) as ln(a) + ln(1 + b / a)
fn sum(a: Log, b: Log) -> f64 {
    if a.0 > b.0 {
        if b.is_zero() {
            a.0
        } else {
            a.0 + ln_one_plus_div(a.0, b.0)
        }
    } else {
        if a.is_zero() {
            b.0
        } else {
            b.0 + ln_one_plus_div(b.0, a.0)
        }
    }
}

impl_op!(+ |a: Log, b: Log| -> Log {
    Log(sum(a, b))
});

impl_op!(+= |a: &mut Log, b: Log| {
    a.0 = sum(*a, b);
});

// Approximate ln(1 - b / a), where alpha=ln(a), beta=ln(b), a > b
fn ln_one_minus_div(alpha: f64, beta: f64) -> f64 {
    let x = (beta - alpha).exp();
    if x < APPROXIMATION_THRESHOLD {
        -x
    } else {
        (1.0 - x).ln()
    }
}

// Approximate ln(a - b) as ln(a) + ln(1 - b / a)
fn difference(a: Log, b: Log) -> f64 {
    assert!(a.0 >= b.0, "Cannot subtract {:?} from {:?}", b, a);
    if b.is_zero() {
        a.0
    } else {
        a.0 + ln_one_minus_div(a.0, b.0)
    }
}

impl_op!(- |a: Log, b: Log| -> Log {
    Log(difference(a, b))
});

impl_op!(-= |a: &mut Log, b: Log| {
    a.0 = difference(*a, b);
});

impl iter::Sum<Log> for Log {
    fn sum<I: Iterator<Item = Log>>(iter: I) -> Log {
        iter.fold(Log::ZERO, |a, b| a + b)
    }
}

impl<'a> iter::Sum<&'a Log> for Log {
    fn sum<I: Iterator<Item = &'a Log>>(iter: I) -> Log {
        iter.fold(Log::ZERO, |a, &b| a + b )
    }
}

impl iter::Product for Log {
    fn product<I: Iterator<Item = Log>>(iter: I) -> Log {
        iter.fold(Log::ONE, |a, b| a * b)
    }
}

pub fn phred_score(values: &[Log]) -> f64 {
    let mut success = match values.iter().max() {
        Some(value) => *value,
        None => return 0.0,
    };

    let mut failure = Log::ZERO;
    for &value in values.iter() {
        if value == success {
            success = Log::ZERO;
        } else {
            failure += value;
        }
    }
    -10.0 * failure.log10()
}

pub fn phred_from_ratio(ratio: Log) -> f64 {
    10.0 * (ratio + Log::ONE).log10()
}

pub fn normalize(values: &mut [Log]) {
    let sum: Log = values.iter().sum();
    values.iter_mut().for_each(|x| *x /= sum);
}

#[cfg(test)]
mod tests {
    use utils::Log;

    fn approx_eq_f64_by(a: f64, b: f64, threshold: f64) -> bool {
        if b == 0.0 {
            a.abs() <= threshold
        } else {
            (a - b).abs() <= threshold && (a / b - 1.0).abs() <= threshold
        }
    }

    fn approx_eq_f64(a: f64, b: f64) -> bool {
        approx_eq_f64_by(a, b, 1e-12)
    }

    fn approx_eq_log(a: Log, b: Log) -> bool {
        approx_eq_f64(a.real(), b.real()) && (a.is_zero() || approx_eq_f64(a.ln(), b.ln()))
    }

    fn assert_approx_log(a: Log, b: Log) {
        assert!(approx_eq_log(a, b), "left != right:\nleft: {:?}\nright: {:?}", a, b);
    }

    fn assert_approx_f64(a: f64, b: f64) {
        assert!(approx_eq_f64(a, b), "left != right:\nleft: {:?}\nright: {:?}", a, b);
    }

    fn assert_approx_f64_by(a: f64, b: f64, threshold: f64) {
        assert!(approx_eq_f64_by(a, b, threshold), "left != right:\nleft: {:?}\nright: {:?}", a, b);
    }

    #[test]
    fn log_general() {
        let x = Log::from_real(100.0);
        assert_approx_f64(x.real(), 100.0);
        assert_approx_f64(x.log10(), 2.0);
        assert_approx_log(x, Log::from_log10(2.0));

        let x = Log::from_real(0.01);
        assert_approx_f64(x.real(), 0.01);
        assert_approx_f64(x.log10(), -2.0);
        assert_approx_log(x, Log::from_log10(-2.0));
    }

    #[test]
    fn log_operations() {
        let x = Log::from_real(0.1);
        let y = Log::from_real(0.2);
        assert_approx_log(x + y, Log::from_real(0.3));
        assert_approx_log(x * y, Log::from_real(0.02));
        assert_approx_log(y - x, Log::from_real(0.1));
        assert_approx_log(x / y, Log::from_real(0.5));
        assert_approx_log(y / x, Log::from_real(2.0));
        assert_approx_log(x.pow(2), Log::from_real(0.01));
        assert_approx_log(x.pow(-2), Log::from_real(100.0));
    }

    #[test]
    fn log_sum() {
        let x = 0.1;
        let x_log = Log::from_real(x);
        for i in 0..11 {
            let y = 10.0_f64.powi(-i);
            let y_log = Log::from_real(y);
            assert_approx_f64_by((x_log + y_log).real(), x + y, 5e-6);
            let mut z = x_log;
            z += y_log;
            assert_approx_log(x_log + y_log, z);
            if y <= x {
                assert_approx_f64_by((x_log - y_log).real(), x - y, 5e-6);
                let mut z = x_log;
                z -= y_log;
                assert_approx_log(x_log - y_log, z);
            }
        }

        assert_approx_log(x_log + Log::ZERO, x_log);
        assert_approx_log(Log::ZERO + x_log, x_log);
        let mut x1 = x_log;
        x1 += Log::ZERO;
        assert_approx_log(x_log, x1);

        assert_approx_log(Log::ZERO + Log::ZERO, Log::ZERO);
        assert_approx_log(Log::ZERO - Log::ZERO, Log::ZERO);
    }
}