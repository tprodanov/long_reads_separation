use std::io::{Write, Result, BufWriter, stderr};
use std::fs::File;
use std::time::Instant;
use std::panic;
use std::thread;
use std::path::Path;

use slog::{Drain, Duplicate, Logger, Level, LevelFilter, Discard, Fuse};
use slog_term::Decorator;
use slog_scope::GlobalLoggerGuard;

use utils::common::write_duration;

pub fn get_level_name(name: &str) -> Option<Level> {
    match name {
        "trace" => Some(Level::Trace),
        "debug" => Some(Level::Debug),
        "info" => Some(Level::Info),
        "warning" => Some(Level::Warning),
        "error" => Some(Level::Error),
        "critical" => Some(Level::Critical),
        "none" => None,
        _ => panic!("Unexpected logging level: {}", name),
    }
}

pub fn create_terminal_drain(timer: Instant, level: Level)
        -> Fuse<LevelFilter<Fuse<slog_async::Async>>> {
    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::CompactFormat::new(decorator)
        .use_custom_timestamp(move |f: &mut dyn Write| -> Result<()>
            { write_duration(f, &timer.elapsed()) })
        .build().fuse();
    let drain = slog_async::Async::new(drain).chan_size(1000).build().fuse();
    LevelFilter::new(drain, level).fuse()
}

/// Ignores KV pairs.
pub struct VeryCompactFormat<D: Decorator> {
    decorator: D,
}

impl<D: Decorator> VeryCompactFormat<D> {
    fn new(decorator: D) -> Self {
        Self { decorator }
    }
}

const LOG_LEVEL_SHORT_NAMES: [&'static str; 7] = ["OFF", "CRIT", "E", "W", "I", "D", "T"];

impl<D: Decorator> Drain for VeryCompactFormat<D> {
    type Ok = ();
    type Err = std::io::Error;

    fn log(&self, record: &slog::Record, values: &slog::OwnedKVList) -> std::result::Result<Self::Ok, Self::Err> {
        self.decorator.with_record(record, values, |decorator| {
            decorator.start_level()?;
            write!(decorator, "{}", LOG_LEVEL_SHORT_NAMES[record.level().as_usize()])?;

            decorator.start_whitespace()?;
            write!(decorator, " ")?;

            decorator.start_msg()?;
            write!(decorator, "{}", record.msg())?;

            // {
            //     let mut serializer = slog_term::Serializer::new(decorator, true, false);
            //     record.kv().serialize(record, &mut serializer)?;
            //     serializer.finish()?;
            // }

            decorator.start_whitespace()?;
            writeln!(decorator)?;
            decorator.flush()?;
            Ok(())
        })
    }
}

pub fn create_file_drain<P: AsRef<Path>>(path: P, level: Level) -> Fuse<LevelFilter<Fuse<slog_async::Async>>> {
    let path = path.as_ref();
    let file = BufWriter::new(File::create(&path)
        .unwrap_or_else(|_| panic!("Failed to create a log file '{}'", path.display())));
    let decorator = slog_term::PlainDecorator::new(file);

    let drain = VeryCompactFormat::new(decorator).fuse();
    let chan_size = if level == Level::Trace {
        10_000_000
    } else {
        10_000
    };
    let drain = slog_async::Async::new(drain).chan_size(chan_size).build().fuse();
    LevelFilter::new(drain, level).fuse()
}

pub fn create<P: AsRef<Path>>(timer: &Instant, terminal: Option<Level>,
        log_file: Option<(P, Level)>) -> Logger {
    let drain1 = terminal.map(|level| create_terminal_drain(timer.clone(), level));
    let drain2 = log_file.map(|path_level| create_file_drain(path_level.0, path_level.1));

    match (drain1, drain2) {
        (Some(d1), Some(d2)) => Logger::root(Duplicate(d1, d2).fuse(), o!()),
        (Some(d1), None) => Logger::root(d1, o!()),
        (None, Some(d2)) => Logger::root(d2, o!()),
        (None, None) => Logger::root(Discard, o!()),
    }
}

pub fn hook_panic() {
    panic::set_hook(Box::new(|info| {
        let thread = thread::current();
        let thread = thread.name().unwrap_or("unnamed");

        let msg = match info.payload().downcast_ref::<&'static str>() {
            Some(s) => *s,
            None => match info.payload().downcast_ref::<String>() {
                Some(s) => &**s,
                None => "Box<Any>",
            },
        };

        match info.location() {
            Some(location) => {
                crit!("thread '{}' panicked at {}:{}\n    {}",
                    thread,
                    location.file(),
                    location.line(),
                    msg,
                );
            }
            None => {
                crit!("thread '{}' panicked at\n    {}",
                    thread,
                    msg,
                );
            }
        }

        if slog_scope::with_logger(|logger| logger.is_trace_enabled()) {
            trace!("{:?}", backtrace::Backtrace::new());
        }
        eprintln!("Exit due to panic!");
        let _ignore = stderr().flush();
        thread::sleep(std::time::Duration::from_millis(100));
        std::process::exit(1);
    }));
}

pub fn register(logger: Logger) -> GlobalLoggerGuard {
    let scope_guard = slog_scope::set_global_logger(logger);
    hook_panic();
    scope_guard
}
