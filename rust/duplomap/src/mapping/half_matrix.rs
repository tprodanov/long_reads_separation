
pub struct HalfMatrix<T> {
    data: Vec<T>,
    side: usize,
}

impl<T: Default> HalfMatrix<T> {
    pub fn new(side: usize) -> HalfMatrix<T> {
        Self::new_by(side, || T::default())
    }
}

impl<T: Clone> HalfMatrix<T> {
    pub fn new_with(side: usize, value: T) -> HalfMatrix<T> {
        assert!(side > 0, "Matrix should have positive size");
        HalfMatrix {
            data: vec![value; side * (side - 1) / 2],
            side: side,
        }
    }
}

impl<T> HalfMatrix<T> {
    pub fn new_by<F>(side: usize, constructor: F) -> HalfMatrix<T>
where F: Fn() -> T {
        assert!(side > 0, "Matrix should have positive size");
        let mut data: Vec<_> = (0..side * (side - 1) / 2).map(|_| constructor()).collect();
        data.shrink_to_fit();
        HalfMatrix { data, side }
    }

    pub fn side(&self) -> usize {
        self.side
    }

    pub fn len(&self) -> usize {
        self.side * (self.side - 1) / 2
    }

    fn linear_index(&self, i: usize, j: usize) -> usize {
        self.side * (self.side - 1) / 2 - (self.side - i) * (self.side - i - 1) / 2 + j - i - 1
    }

    pub fn at(&self, i: usize, j: usize) -> &T {
        assert!(i < j, "Half matrix: i should be smaller than j ({} >= {})", i, j);
        assert!(j < self.side, "Index out of range {} >= {}", j, self.side);
        &self.data[self.linear_index(i, j)]
    }

    pub fn at_mut(&mut self, i: usize, j: usize) -> &mut T {
        assert!(i < j, "Half matrix: i should be smaller than j ({} >= {})", i, j);
        assert!(j < self.side, "Index out of range {} >= {}", j, self.side);
        let k = self.linear_index(i, j);
        &mut self.data[k]
    }

    pub fn values(&self) -> std::slice::Iter<T> {
        self.data.iter()
    }

    pub fn indices(&self) -> IndexIter {
        IndexIter {
            i: 0,
            j: 0,
            side: self.side,
        }
    }

    pub fn enumerate(&self) -> EnumerateIter<T> {
        EnumerateIter {
            index_iter: self.indices(),
            parent: self,
        }
    }
}

pub struct IndexIter {
    i: usize,
    j: usize,
    side: usize,
}

impl Iterator for IndexIter {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        if self.j + 1 < self.side {
            self.j += 1;
        } else if self.i + 2 < self.side {
            self.i += 1;
            self.j = self.i + 1;
        } else {
            return None;
        }
        Some((self.i, self.j))
    }
}

pub struct EnumerateIter<'a, T> {
    index_iter: IndexIter,
    parent: &'a HalfMatrix<T>,
}

impl<'a, T> Iterator for EnumerateIter<'a, T> {
    type Item = (usize, usize, &'a T);

    fn next(&mut self) -> Option<Self::Item> {
        self.index_iter.next().map(|(i, j)| (i, j, self.parent.at(i, j)))
    }
}
