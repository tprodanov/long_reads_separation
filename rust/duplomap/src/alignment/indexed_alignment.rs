use std::cmp::min;
use std::sync::Arc;

use bio::utils::{Text, TextSlice};

use alignment::Alignment;
use alignment::hmm;
use alignment::alignment::Stats;
use biology::Genome;
use biology::spans::{Span, SpanTools};
use utils::Log;

pub struct IndexedAlignment {
    query_sequence: Arc<bam::record::Sequence>,
    alignment: Alignment,
    ref_to_query: Vec<u32>,
    stats: Stats,
}

impl IndexedAlignment {
    pub fn new(query_sequence: Arc<bam::record::Sequence>, ref_sequence: TextSlice,
            alignment: Alignment, genome: &Genome, hmm_params: &hmm::Parameters)
            -> IndexedAlignment {
        assert!(query_sequence.len() as u32 == alignment.query_len(),
            "Alignment and query have different lengths: {} != {}",
            alignment.query_len(), query_sequence.len());
        assert!(ref_sequence.len() as u32 == alignment.aln_span().len(),
            "Alignment and reference have different lengths: {} != {}",
            alignment.aln_span().len(), ref_sequence.len());

        let mut ref_to_query = Vec::new();
        let mut current_q_pos = 0;
        for (q_pos, r_pos) in alignment.get_aligned_pairs() {
            if let Some(value) = q_pos {
                current_q_pos = value;
            }
            if let Some(_value) = r_pos {
                ref_to_query.push(current_q_pos as u32);
            }
        }
        assert!(ref_to_query.len() as u32 == alignment.aln_span().len());
        ref_to_query.shrink_to_fit();
        let stats = alignment.get_stats(&query_sequence, alignment.strand(),
            ref_sequence, hmm_params);
        alignment.write_stats(&stats, genome);

        IndexedAlignment { query_sequence, alignment, ref_to_query, stats }
    }

    pub fn alignment(&self) -> &Alignment {
        &self.alignment
    }

    pub fn score(&self) -> f64 {
        self.stats.score
    }

    pub fn probability(&self) -> Log {
        self.stats.probability
    }

    // Returns starting position and the subsequence.
    pub fn subsequence(&self, span: &impl Span) -> (u32, Text) {
        let alignment_span = self.alignment.aln_span();
        assert!(alignment_span.intersects(span),
            "Cannot extract subsequence: PSV does not intersect the read");

        let aln_start = alignment_span.start();
        let mut start = self.ref_to_query[span.start().saturating_sub(aln_start) as usize];
        let mut end = self.ref_to_query[min(span.end().saturating_sub(aln_start) as usize,
            self.ref_to_query.len()).saturating_sub(1)] + 1;
        let query_range = self.alignment.query_range();
        if span.start() < aln_start && query_range.start() > 0 {
            start = start.saturating_sub(aln_start - span.start());
        }
        if span.end() > alignment_span.end() && query_range.end() < self.query_sequence.len() as u32 {
            end = min(end + span.end() - alignment_span.end(), self.query_sequence.len() as u32);
        }

        let (q_start, q_end) = if self.alignment.strand() {
            (start as usize, end as usize)
        } else {
            let len = self.query_sequence.len();
            (len - end as usize, len - start as usize)
        };

        let seq = if span.strand() == self.alignment.strand() {
            self.query_sequence.subseq_acgtn_only(q_start..q_end).collect()
        } else {
            self.query_sequence.rev_compl_acgtn_only(q_start..q_end).collect()
        };
        (start, seq)
    }
}