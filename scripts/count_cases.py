#!/usr/bin/env python3

import csv
import sys
import re
import gzip
from collections import Counter, defaultdict


def natural_key(s):
    def convert(text):
        return int(text) if text.isdigit() else text.lower()
    return [convert(c) for c in re.split('([0-9]+)', s)]


class Region:
    def __init__(self, s):
        m = re.match(r'([^:]+):([0-9]+)-([0-9]+)', s)
        self.chr = m.group(1)
        self.start = int(m.group(2)) - 1
        self.end = int(m.group(3))

    def intersection(self, oth):
        if self.chr != oth.chr:
            return 0
        return max(0, min(self.end, oth.end) - max(self.start, oth.start))

    def __len__(self):
        return self.end - self.start


def compare_locations(locs1, locs2):
    locs1 = [Region(s) for s in locs1.split(',')]
    locs2 = [Region(s) for s in locs2.split(',')]
    l1 = sum(map(len, locs1))
    l2 = sum(map(len, locs2))

    intersection = 0
    for loc1 in locs1:
        for loc2 in locs2:
            intersection += loc1.intersection(loc2)
    return intersection / min(l1, l2)


def get_by_alias(d, *aliases):
    for key in aliases:
        if key in d:
            return d[key]
    raise KeyError('None of the key %s are in the dictionary' % aliases)


def process(reader):
    res = defaultdict(Counter)
    for row in reader:
        inters = compare_locations(row['aln_before'], row['aln_after'])
        loc = 'same' if inters > 0.5 else 'diff'
        m1 = int(row['mapq_before']) >= 30
        m2 = int(row['mapq_after']) >= 30
        if m1 and m2:
            mapq = 'high->high'
        elif m1 and not m2:
            mapq = 'high->low'
        elif not m1 and m2:
            mapq = 'low->high'
        else:
            mapq = 'low->low'
        key = (loc, mapq, row.get('reason', 'unknown'))
        res[row['database']][key] += 1
        if get_by_alias(row, 'primary', 'first_entry') == 'true':
            res[''][key] += 1
    return res


def main():
    inp = gzip.open(sys.argv[1], 'rt')
    reader = csv.DictReader(inp, delimiter='\t')
    res = process(reader)
    print('component\tlocation\tmapq\treason\tcount\tperc')
    for db in sorted(res, key=natural_key):
        db_counter = res[db]
        if not db:
            db = 'full'
        total = sum(db_counter.values())
        for (loc, mapq, reason), count in sorted(db_counter.items()):
            print('%s\t%s\t%s\t%s\t%d\t%.1f' % (db, loc, mapq, reason, count, 100 * count / total))


if __name__ == '__main__':
    main()

