use std::sync::{Weak, RwLock, Mutex};

use slog::{Logger, Drain};
use slog_scope;
use bam;

use duplomap::biology::{Genome, IdConverter};
use duplomap::database::HeavyDatabase;
use duplomap::database::genotype::GenotypeUpdate;
use duplomap::mapping::{HeavyRead, Parameters};
use duplomap::utils::init_logger;
use duplomap::utils::common::{MatchesMap, Matches};
use duplomap::utils::thread_pool::{Execute, Job};

type WeakMutexVec<T> = Weak<Mutex<Vec<T>>>;

#[derive(Default)]
struct Output {
    reads: WeakMutexVec<HeavyRead>,
    genotype_updates: WeakMutexVec<GenotypeUpdate>,
}

impl Output {
    fn push_read(&mut self, read: HeavyRead) {
        self.reads.upgrade().expect("Worker::reads are not defined")
            .lock().expect("Stop due to panic in another thread")
            .push(read);
    }

    fn extend_genotype_updates(&mut self, updates: Vec<GenotypeUpdate>) {
        self.genotype_updates.upgrade().expect("Worker::genotype_updates are not defined")
            .lock().expect("Stop due to panic in another thread")
            .extend(updates);
    }
}

pub struct Worker {
    genome: Genome,
    params: Parameters,
    logger: Option<Logger>,
    output: Output,
    database: Weak<RwLock<HeavyDatabase>>,
    id_converter: IdConverter,
}

impl Worker {
    pub fn new(index: u16, terminal_logger: Logger, matches: &MatchesMap,
            params: Parameters, id_converter: IdConverter) -> Self {
        let logger = match init_logger::get_level_name(matches.get_str("log")) {
            Some(level) => {
                let output = std::path::Path::new(matches.get_str("output"));
                let file_drain = init_logger::create_file_drain(
                    output.join("logs").join(format!("worker{}.log", index + 1)), level);
                Logger::root(slog::Duplicate(terminal_logger, file_drain).fuse(), o!())
            },
            None => terminal_logger,
        };
        Self {
            genome: Genome::from_file(matches.get_str("reference")),
            params,
            logger: Some(logger),
            output: Output::default(),
            database: Weak::new(),
            id_converter,
        }
    }

    pub fn set_database(&mut self, database: Weak<RwLock<HeavyDatabase>>) {
        self.database = database;
    }

    pub fn set_output(&mut self, reads: WeakMutexVec<HeavyRead>,
            genotype_updates: WeakMutexVec<GenotypeUpdate>,) {
        self.output.reads = reads;
        self.output.genotype_updates = genotype_updates;
    }

    pub fn align_initially(&mut self, record: bam::Record) {
        let db = self.database.upgrade().expect("Worker: Database is dropped");
        let db = db.try_read().expect("Failed to unlock database");
        let read_res =  HeavyRead::new(record, &db, &mut self.genome, &self.id_converter, &self.params);

        let mut read = match read_res {
            Ok(value) => value,
            Err(e) => {
                error!("{}", e);
                return;
            }
        };
        read.update_location_probabilities(&db, &mut self.genome, &self.params, 0);
        let updates = read.update_genotype_probabilities(&db, &self.params);
        self.output.push_read(read);
        self.output.extend_genotype_updates(updates);
    }

    pub fn update_locations_and_genotypes(&mut self, mut read: HeavyRead, iteration: u32) {
        let db = self.database.upgrade().expect("Worker: Database is dropped");
        let db = db.try_read().expect("Failed to unlock database");
        read.update_location_probabilities(&db, &mut self.genome, &self.params, iteration);
        let updates = read.update_genotype_probabilities(&db, &self.params);
        self.output.push_read(read);
        self.output.extend_genotype_updates(updates);
    }
}

impl Execute for Worker {
    fn execute(&mut self, function: Job<Self>) {
        match self.logger.take() {
            Some(logger) => {
                slog_scope::scope(&logger, || function.call_box(self));
                self.logger = Some(logger);
            },
            None => {
                function.call_box(self);
            },
        }
    }
}