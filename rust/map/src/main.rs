#[macro_use] extern crate slog;
#[macro_use] extern crate slog_scope;
#[macro_use] extern crate clap;
extern crate csv;
#[macro_use] extern crate serde;
extern crate bam;

#[macro_use] extern crate duplomap;

mod worker;

use std::path::Path;
use std::fs::{self, File};
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, RwLock, Mutex};
use std::io::{self, Write};
use std::thread;
use std::time::{Duration, Instant};
use std::ops::DerefMut;
use std::str::FromStr;

use clap::App;
use slog::Drain;
use bam::{RecordWriter, RecordReader};

use worker::Worker;
use duplomap::biology;
use duplomap::biology::spans::Span;
use duplomap::database;
use duplomap::alignment;
use duplomap::mapping;
use duplomap::utils::{vcf, Log, ThreadPool, Narrator, Observer};
use duplomap::utils::common::{self, Matches, MatchesMap, CsvGzWriter, GzWriter};
use duplomap::utils::init_logger;
use duplomap::utils::vcf::VcfWrite;

fn check_executable(path: &str) {
    let mut command = std::process::Command::new(path);
    command.arg("--version");
    let output = match command.output() {
        Err(e) => {
            crit!("Failed to run {}", path);
            panic!("    Command {:?} failed with: {}", command, e);
        },
        Ok(value) => value,
    };
    if !output.status.success() {
        crit!("Failed to run {}", path);
        let stderr = String::from_utf8_lossy(&output.stderr);
        panic!("    Command {:?} failed with stderr: {}", command, stderr);
    }
}

fn parse_parameters(matches: &MatchesMap, hmm_params: alignment::hmm::Parameters, use_m: bool)
        -> mapping::Parameters {
    let skip_mapq = if &matches.get_str("skip-mapq").to_lowercase() == "none" {
        None
    } else {
        Some(matches.get_value("skip-mapq"))
    };
    let n_secondary = if &matches.get_str("secondary").to_lowercase() == "all" {
        std::u16::MAX
    } else {
        matches.get_value("secondary")
    };
    let psv_complexity = matches.get_vec_or_default("psv-complexity", vec![0.6, 0.8]);
    let ambiguous = matches.get_vec_or_default("ambiguous", vec![4.0, 30.0]);

    let preset = match matches.get_str("preset") {
        "pacbio" | "pb" => alignment::c_minimap::Preset::PacBio,
        "nanopore" | "ont" => alignment::c_minimap::Preset::Nanopore,
        value => panic!("Unexpected preset {}", value),
    };

    mapping::Parameters {
        psv_sub_complexity: psv_complexity[0],
        psv_indel_complexity: psv_complexity[1],
        psv_size_diff: matches.get_value("psv-size-diff"),

        priors: mapping::parameters::Priors::new(matches.get_value("gt-prior")),
        read_psv_impact: Log::from_log10(-matches.get_value::<f64, _>("read-psv-impact")),
        genotyping_min_mapq: matches.get_value("gt-min-mapq"),
        ambiguous_fold_diff: ambiguous[0],
        ambiguous_reads: ambiguous[1] / 100.0,
        high_cn_perc: matches.get_value("copy-num-perc"),

        relative_padding: matches.get_value("relative-padding"),
        unknown_regions: mapping::parameters::UnknownRegions::from_str(
            matches.get_str("unknown-regions")).unwrap(),
        filtering_kmer: matches.get_value("filtering-kmer"),
        filtering_p_value: matches.get_value("filtering-p-value"),
        max_locations: matches.get_value("max-locations"),

        minimap_kmer_size: matches.get_value("minimap-kmer"),
        minimap_preset: preset,

        conflicts_p_value: matches.get_value("conflicts-p-value"),
        min_conflicts: std::cmp::max(1, matches.get_value("min-conflicts")),
        hmm_params,
        skip_mapq,
        use_m_in_cigar: use_m,
        n_secondary,
    }
}

formatted!(Precision1, "{:.1}");
formatted!(Precision2, "{:.2}");

#[derive(Serialize)]
struct CoveragesRow<'a> {
    database: &'a str,
    window_id: usize,
    chrom1: &'a str,
    pos1: u32,
    similarity1: Precision1<f64>,
    region1_id: &'a str,
    chrom2: &'a str,
    pos2: u32,
    similarity2: Precision1<f64>,
    region2_id: &'a str,
    cov1: Precision2<f64>,
    cov2: Precision2<f64>,
}

fn write_window_coverages<P: AsRef<Path>>(path: P,
        coverages: &HashMap<database::heavy_database::WindowId, [f64; 2]>,
        db: &database::HeavyDatabase, genome: &biology::Genome)
        -> io::Result<()> {
    let mut writer = csv::WriterBuilder::new()
        .delimiter(b'\t')
        .from_path(path)?;
    for (window_id, coverages) in coverages {
        let window = db.window_at(*window_id);
        let regions: Vec<_> = window.iter()
            .map(|span| db.find_region_ids(span).next()
            .map(|i| i.index().to_string()).unwrap_or_else(|| "NA".to_string())).collect();
        let similarities: Vec<_> = window.iter().map(|span| db.window_similarity(span)).collect();

        for i in 0..2 {
            let row = CoveragesRow {
                database: db.name(),
                window_id: window_id.index(),
                chrom1: genome.chrom_name(window[i].chrom_id()),
                pos1: window[i].start(),
                similarity1: Precision1(similarities[i]),
                region1_id: &regions[i],
                chrom2: genome.chrom_name(window[1 - i].chrom_id()),
                pos2: window[1 - i].start(),
                similarity2: Precision1(similarities[1 - i]),
                region2_id: &regions[1 - i],
                cov1: Precision2(coverages[i]),
                cov2: Precision2(coverages[1 - i]),
            };
            writer.serialize(row)?;
        }
    }
    Ok(())
}

fn create_header(header: &bam::Header) -> bam::Header {
    let mut header = header.clone();
    let mut entry = bam::header::HeaderEntry::program("duplomap".to_string());
    entry.push(b"VN", crate_version!().to_string());
    entry.push(b"CL", std::env::args().collect::<Vec<_>>().join(" "));
    header.push_entry(entry).expect("Cannot create BAM header");
    header
}

struct MappingStructures<'a> {
    matches: &'a MatchesMap,
    params: &'a mapping::Parameters,
    pool: &'a mut ThreadPool<Worker>,
    observer: &'a Observer,
    id_converter: &'a biology::IdConverter,
    reader: bam::IndexedReader<File>,
    writer: bam::BamWriter<File>,
    genome: &'a mut biology::Genome,
    read_psv_writer: Option<CsvGzWriter>,
    psv_writer: Option<GzWriter>,
    conflict_rate: Option<f64>,
    vcf_writer: vcf::Writer<File>,
}

// Returns true if any reads changed locations/mapq.
fn have_changed_reads(before: &[(String, Option<(u16, u8)>)], after: &[(String, Option<(u16, u8)>)],
        print_changes: bool) -> bool {
    let mut changed_locs = 0;
    let mut increased_mapq = 0;
    let mut decreased_mapq = 0;
    for (prev, curr) in before.iter().zip(after.iter()) {
        let name = &prev.0;
        assert!(name == &curr.0, "Reads do not match for consecutive iterations");
        if prev.1 == curr.1 {
            continue;
        }

        match (prev.1, curr.1) {
            (Some((prev_loc, prev_mapq)), Some((curr_loc, curr_mapq))) => {
                if prev_loc != curr_loc {
                    changed_locs += 1;
                } else if prev_mapq < 30 && curr_mapq >= 30 {
                    increased_mapq += 1;
                } else if prev_mapq >= 30 && curr_mapq < 30 {
                    decreased_mapq += 1;
                } else {
                    continue;
                }
            },
            (None, Some((_, curr_mapq))) => {
                if curr_mapq >= 30 {
                    increased_mapq += 1;
                } else {
                    continue;
                }
            },
            (Some((_, prev_mapq)), None) => {
                if prev_mapq >= 30 {
                    decreased_mapq += 1;
                } else {
                    continue;
                }
            },
            _ => unreachable!(),
        }
        if print_changes {
            debug!("            Read {}: {:?} -> {:?}", name, prev.1, curr.1);
        }
    }
    let changed_reads = changed_locs + increased_mapq + decreased_mapq;
    debug!("    {} reads changed mappings", changed_reads);
    if changed_locs > 0 {
        debug!("        {} reads switched locations", changed_locs);
    }
    if increased_mapq > 0 {
        debug!("        {} reads retained locations and increased MAPQ", increased_mapq);
    }
    if decreased_mapq > 0 {
        debug!("        {} reads retained locations and decreased MAPQ", decreased_mapq);
    }
    changed_reads > 0
}

fn map_to_database(db: database::database::NamedDatabase, args: &mut MappingStructures) -> bool {
    debug!("Component {}", db.name);
    args.observer.prefix(db.name.clone());

    args.observer.message("Loading reads".to_string());
    let mut records = mapping::input_bam::fetch(&mut args.reader, &db.database.duplicated_regions,
        args.id_converter, args.genome);
    debug!("    Found {} reads for component {}", records.len(), db.name);
    if records.is_empty() {
        debug!("No reads available for the component {}. Skipping", db.name);
        args.observer.pause();
        return false;
    }

    let output = Path::new(args.matches.get_str("output")).join(&db.name);
    common::create_dir(&output, args.matches.contains("continue"))
        .expect("Failed to create an output subdirectory");
    args.observer.message("Loading PSVs".to_string());
    let db = db.to_heavy(args.genome, args.params);

    if args.matches.contains("first") {
        let count_reads: usize = args.matches.get_nth_value("first", 1);
        if count_reads != 0 {
            records.sort_by(|a, b| a.name().cmp(&b.name()));
            records.truncate(count_reads);
            debug!("    Selected first {} reads", records.len());
        }
    }

    let mut coverages = HashMap::new();
    let generated = args.matches.contains("generated");
    let mut information = csv::WriterBuilder::new().delimiter(b'\t')
        .from_path(output.join("information.csv"))
        .expect("Failed to create temporary file for information.csv");

    let db_name = db.name().to_string();
    let db = Arc::new(RwLock::new(db));
    args.pool.reset_finished_jobs();
    let n_jobs = records.len() as u32;

    debug!("    Looking for possible locations and aligning to them");
    let reads = Arc::new(Mutex::new(Vec::new()));
    let genotype_updates = Arc::new(Mutex::new(Vec::new()));
    args.pool.for_each(|worker| {
        worker.set_output(Arc::downgrade(&reads), Arc::downgrade(&genotype_updates));
        worker.set_database(Arc::downgrade(&db));
    });

    args.observer.message("Aligning to possible locations".to_string());
    let finished_jobs = args.pool.finished_jobs();
    args.observer.progress(0, n_jobs);

    // First iteration: find locations, create
    for record in records.into_iter() {
        args.pool.execute(|worker: &mut Worker| worker.align_initially(record));
    }

    let mut out_record = bam::Record::new();
    loop {
        if args.pool.finished_at_least(n_jobs) {
            break;
        } else {
            thread::sleep(Duration::from_millis(100));
            args.observer.progress(finished_jobs.load(std::sync::atomic::Ordering::Relaxed), n_jobs);
        }
    }
    std::mem::drop(finished_jobs);
    db.try_write().expect("Failed to unlock database").update_genotypes(
        &*genotype_updates.try_lock().expect("Genotype updates should be unlocked"),
        &args.params, &args.genome, "0", &mut args.psv_writer);
    db.try_write().expect("Failed to unlock database").select_active_psvs(args.params.ambiguous_reads);

    let iterations: u32 = args.matches.get_value("iterations");
    args.observer.remove_progress();

    let mut prev_locations: Vec<_> = reads.try_lock().expect("Reads should be unlocked")
        .iter().map(|read| (read.name().to_string(), read.best_location())).collect();
    prev_locations.sort();
    for iteration in 0..iterations {
        debug!("{}. Updating genotype probabilities. Iteration {}", db_name, iteration + 1);
        args.observer.message(format!("Updating locations and genotypes. Iteration {}", iteration + 1));
        let old_reads = std::mem::replace(reads.try_lock().expect("Reads should be unlocked").deref_mut(), Vec::new());
        genotype_updates.try_lock().expect("Genotype updates should be unlocked").clear();
        args.pool.reset_finished_jobs();
        let n_jobs = old_reads.len() as u32;

        for read in old_reads.into_iter() {
            args.pool.execute(move |worker: &mut Worker| worker.update_locations_and_genotypes(read, iteration + 1));
        }
        while !args.pool.finished_at_least(n_jobs) {
            thread::sleep(Duration::from_millis(100));
        }
        db.try_write().expect("Failed to unlock database").update_genotypes(
            &*genotype_updates.try_lock().expect("Genotype updates should be unlocked"),
            &args.params, &args.genome, &(iteration + 1).to_string(), &mut args.psv_writer);

        let mut new_locations: Vec<_> = reads.try_lock().expect("Reads should be unlocked")
            .iter().map(|read| (read.name().to_string(), read.best_location())).collect();
        new_locations.sort();
        assert!(prev_locations.len() == new_locations.len());

        if iteration >= 10 {
            debug!("    Searching for reads with changed mappings");
        }
        if !have_changed_reads(&prev_locations, &new_locations, iteration >= 10) {
            break;
        }
        prev_locations = new_locations;
    }

    let mut db = db.try_write().expect("Failed to unlock database");
    let mut reads = reads.try_lock().expect("Reads should be unlocked");
    args.observer.message("Estimating read conflicts".to_string());
    debug!("Estimating conflict rate");
    let mut total_conflict_psvs = 0;
    let mut total_appl_psvs = 0;
    let mut total_used_reads = 0;

    const NEED_READS: u32 = 50;
    const MIN_PSVS: u32 = 5;

    for read in reads.iter_mut() {
        let (conflict_psvs, appl_psvs) = read.count_conflicts(&db, &args.genome, &args.params,
            &mut args.read_psv_writer);
        if appl_psvs >= MIN_PSVS {
            total_used_reads += 1;
            total_appl_psvs += appl_psvs;
            total_conflict_psvs += conflict_psvs;
        }
    }
    let mut conflict_rate = total_conflict_psvs as f64 / total_appl_psvs as f64;
    debug!("    {} reads with at least {} appropriate PSVs. Conflicts rate = {}/{} = {:.1}%",
        total_used_reads, MIN_PSVS, total_conflict_psvs, total_appl_psvs, 100.0 * conflict_rate);

    const MIN_CONFLICT_RATE: f64 = 0.001;
    if conflict_rate < MIN_CONFLICT_RATE {
        conflict_rate = MIN_CONFLICT_RATE;
        debug!("    Conflict rate is too low, using {:.1}%", 100.0 * conflict_rate);
    }

    if total_used_reads < NEED_READS {
        debug!("    Too few reads ({} < {}) to estimate conflict rate.", total_used_reads, NEED_READS);
        conflict_rate = args.conflict_rate.unwrap_or(0.0);
        if conflict_rate > 0.0 {
            debug!("    Using rate {:.1}% from the biggest of the previous components", conflict_rate * 100.0);
        }
    };

    if args.params.conflicts_p_value == 0.0 {
        debug!("Conflicts p-value == 0, do not reduce reads MAPQ");
    } else if conflict_rate > 0.0 {
        let p_threshold = args.params.conflicts_p_value / reads.len() as f64;
        debug!("Using Bonferroni correction: p-value <- {:.2e}", p_threshold);
        if args.conflict_rate.is_none() {
            args.conflict_rate = Some(conflict_rate);
        }

        let mut updates = Vec::new();
        for read in reads.iter_mut() {
            read.reduce_mapq_if_conflicting(conflict_rate, p_threshold);
            updates.extend(read.update_genotype_probabilities(&db, &args.params));
        }
        args.observer.message("Updating genotype probabilities".to_string());
        debug!("    Updating genotype probabilities without conflicting reads");
        db.update_genotypes(&updates, &args.params, &args.genome, "f", &mut args.psv_writer);
    }

    args.observer.message("Writing output".to_string());
    for read in reads.iter_mut() {
        if conflict_rate > 0.0 {
            // Updating conflicts the second time.
            read.count_conflicts(&db, &args.genome, &args.params, &mut None);
        }
        read.update_window_coverage_map(&mut coverages, &db);
        read.write_information(&mut information, &db, args.genome, generated)
            .unwrap_or_else(|_| error!("Failed to write information"));
    }
    write_window_coverages(output.join("window_cov.csv"), &mut coverages, &db, args.genome)
        .unwrap_or_else(|e| error!("{}", e));

    for mut read in reads.drain(..) {
        if args.params.n_secondary > 0 {
            read.align_secondary(args.genome, &args.params);
        }
        read.write(&mut out_record, args.id_converter, args.genome, &db_name, &mut args.writer,
            args.params.n_secondary).unwrap_or_else(|e| panic!("{}", e));
    }

    if !args.matches.contains("skip-vcf") {
        debug!("{}. Writing output vcf file", db.name());
        mapping::output_vcf::write_component(&mut args.vcf_writer, &db, args.genome)
            .unwrap_or_else(|e| panic!("{}", e));
    }

    std::mem::drop(information);
    args.vcf_writer.flush().map_err(|_| error!("Cannot flush vcf file")).ok();
    args.writer.flush().map_err(|_| error!("Cannot flush bam file")).ok();
    if let Err(e) = File::create(output.join("success")) {
        error!("Cannot create file {}/success ({})", output.display(), e);
    }
    args.observer.pause();
    true
}

fn create_log_dir(output: &Path) -> io::Result<()> {
    let def_path = output.join("logs");
    if !Path::exists(&def_path) {
        fs::create_dir(&def_path)?;
        return Ok(());
    }
    for i in 0..1000 {
        let path = output.join(format!("logs.{:03}", i));
        if !Path::exists(&path) {
            info!("Renaming old directory with logs {} -> {}", def_path.display(), path.display());
            fs::rename(&def_path, path)?;
            fs::create_dir(&def_path)?;
            return Ok(());
        }
    }
    panic!("Cannot create log directory (all names logs.000 - logs.999 are already used)");
}

fn rewrite_bam(output: &Path, header: bam::Header, completed_dbs: &HashSet<String>) -> io::Result<bam::BamWriter<File>> {
    let bam_name = output.join("unsorted.bam");
    let tmp_name = output.join("unsorted.tmp.bam");

    if Path::exists(&tmp_name) {
        warn!("File {} exists, probably duplomap was stopped during BAM file rewriting.", tmp_name.display());
        warn!("Continuing may yield incorrect results.");
        fs::rename(&tmp_name, &bam_name)?;
    }

    let need_rewriting = Path::exists(&bam_name) && !completed_dbs.is_empty();
    if need_rewriting {
        fs::rename(&bam_name, &tmp_name)?;
    }
    let mut writer = bam::BamWriter::from_path(&bam_name, header)?;
    if !need_rewriting {
        return Ok(writer);
    }

    info!("Rewriting temporary file with realigned reads");
    // Ignore errors from the reader because it may be corrupted at the end.
    let mut reader = match bam::BamReader::from_path(&tmp_name, 0) {
        Ok(value) => value,
        Err(_) => {
            fs::remove_file(&tmp_name)?;
            return Ok(writer)
        },
    };
    let mut record = bam::Record::new();
    while let Ok(true) = reader.read_into(&mut record) {
        if let Some(bam::record::tags::TagValue::String(db_name, _)) = record.tags().get(b"db") {
            let db_name_str = unsafe { std::str::from_utf8_unchecked(db_name) };
            if completed_dbs.contains(db_name_str) {
                writer.write(&record)?;
            }
        }
    }
    std::mem::drop(reader);
    fs::remove_file(&tmp_name)?;
    Ok(writer)
}

fn rewrite_vcf(output: &Path, genome: &biology::Genome, completed_dbs: &HashSet<String>)
        -> io::Result<vcf::Writer<File>> {
    let vcf_name = output.join("unsorted.vcf");
    let tmp_name = output.join("unsorted.tmp.vcf");

    if Path::exists(&tmp_name) {
        warn!("File {} exists, probably duplomap was stopped during VCF file rewriting.", tmp_name.display());
        warn!("Continuing may yield incorrect results.");
        fs::rename(&tmp_name, &vcf_name)?;
    }

    let need_rewriting = Path::exists(&vcf_name) && !completed_dbs.is_empty();
    if need_rewriting {
        fs::rename(&vcf_name, &tmp_name)?;
    }
    let mut writer = mapping::output_vcf::create(output.join("unsorted.vcf"), &genome)?;
    if !need_rewriting {
        return Ok(writer);
    }

    info!("Rewriting temporary file with PSVs");
    // Ignore errors from the reader because it may be corrupted at the end.
    let mut reader = match vcf::Reader::load(&tmp_name) {
        Ok(value) => value,
        Err(_) => {
            fs::remove_file(&tmp_name)?;
            return Ok(writer);
        },
    };
    'outer: loop {
        let record = match reader.read(genome) {
            Ok(value) => value,
            Err(_) => break,
        };
        for key_value in record.info.split(';') {
            if key_value.starts_with("db=") {
                let db_name = &key_value[3..];
                if completed_dbs.contains(db_name) {
                    break;
                } else {
                    continue 'outer;
                }
            }
        }
        writer.write(&record)?;
    }
    std::mem::drop(reader);
    fs::remove_file(&tmp_name)?;
    Ok(writer)
}

fn get_completed_dbs(output: &Path, db_names: &[String], cont: bool) -> HashSet<String> {
    let mut res = HashSet::new();
    if !cont {
        return res;
    }
    for name in db_names.iter() {
        if Path::exists(&output.join(name).join("success")) {
            res.insert(name.clone());
        }
    }
    res
}

fn check_input_existence(matches: &MatchesMap) {
    let input_bam = matches.get_str("input");
    if !Path::exists(Path::new(input_bam)) {
        panic!("Could not find input file \"{}\"", input_bam);
    }
    if !input_bam.ends_with(".bam") {
        warn!("Input file \"{}\" has unexpected extension (expected .bam), it may have incorrect format.", input_bam);
    }
    let input_bam_bai = format!("{}.bai", input_bam);
    if !Path::exists(Path::new(&input_bam_bai)) {
        panic!("Could not find input index file \"{}\", please run \"samtools index {}\"", input_bam_bai, input_bam);
    }

    let genome_file = matches.get_str("reference");
    if !Path::exists(Path::new(genome_file)) {
        panic!("Could not find reference file \"{}\"", genome_file);
    }
    if !(genome_file.ends_with(".fasta") || genome_file.ends_with(".fa") || genome_file.ends_with(".fna")) {
        warn!("Reference file \"{}\" has unexpected extension (expected .fasta, .fa or .fna), \
            it may have incorrect format.", genome_file);
    }
    let genome_file_fai = format!("{}.fai", genome_file);
    if !Path::exists(Path::new(&genome_file_fai)) {
        panic!("Could not find reference index file \"{}\", please run \"samtools faidx {}\"",
            genome_file_fai, genome_file);
    }
}

// TODO REPLACE &Vec<_> with &[_]

fn main() {
    let timer = Instant::now();
    let yaml = load_yaml!("map.yml");
    let authors = env!("CARGO_PKG_AUTHORS").replace(':', ", ");
    let parser = App::from_yaml(yaml)
        .version(&crate_version!()[..])
        .author(&authors as &str);
    let matches = Arc::new(common::matches_to_map(&parser.get_matches()));
    match common::create_main_directory(&matches) {
        Err(e) => panic!("{}", e),
        _ => { },
    }
    let output = Path::new(matches.get_str("output"));
    let threads: u16 = matches.get_value("threads");

    let terminal_logger = slog::Logger::root(
        init_logger::create_terminal_drain(timer.clone(), slog::Level::Info), o!());
    let logger = match init_logger::get_level_name(matches.get_str("log")) {
        Some(level) => {
            create_log_dir(&output).unwrap_or_else(|e| panic!("Failed to create log directory: {}", e));
            let file_drain = init_logger::create_file_drain(output.join("logs").join("main.log"), level);
            slog::Logger::root(slog::Duplicate(terminal_logger.clone(), file_drain).fuse(), o!())
        },
        None => terminal_logger.clone(),
    };
    let _guard = init_logger::register(logger);
    common::greet();
    check_executable(matches.get_str("samtools"));
    check_input_existence(&matches);

    info!("Version {}", &crate_version!());
    common::print_parameters(&matches);

    let mut genome = biology::Genome::from_file(matches.get_str("reference"));
    let mut reader = bam::IndexedReader::build()
        .modification_time(bam::bam_reader::ModificationTime::warn(|e| warn!("{}", e)))
        .from_path(matches.get_str("input"))
        .unwrap_or_else(|e| panic!("Failed to open input bam file or its index: {}", e));
    let header = create_header(reader.header());

    let id_converter = biology::IdConverter::from_bam(&genome, reader.header());
    let count_additional_to_main = id_converter.count_additional_to_main();
    let count_additional = id_converter.count_additional_chroms();
    let count_main_to_additional = id_converter.count_main_to_additional();
    let count_main = id_converter.count_main_chroms();

    if count_additional_to_main < 20 && count_additional_to_main < count_additional / 2 {
        error!("{} out of {} chromosomes in the BAM file correspond to the genome",
            count_additional_to_main, count_additional);
        panic!("Possibly incorrect genome");
    }
    if count_main_to_additional < 20 && count_main_to_additional < count_main / 2 {
        error!("{} out of {} chromosomes in the genome correspond to the BAM file",
            count_main_to_additional, count_main);
        panic!("Possibly incorrect genome");
    }

    let (hmm_params, use_m) = if matches.contains("default-hmm") {
        info!("Using default HMM parameters");
        (alignment::hmm::Parameters::default(),
            alignment::estimate_params::check_if_cigars_use_m(reader.full(), 100_000))
    } else {
        const MAPQ_THRESHOLD: u8 = 30;
        const N_READS: usize = 100_000;
        alignment::estimate_params::estimate(reader.full(), &id_converter, &mut genome,
            MAPQ_THRESHOLD, N_READS)
    };
    hmm_params.debug_print();
    debug!("Read CIGARs use: {}", if use_m { "M" } else { "X and =" });
    let params = parse_parameters(&matches, hmm_params, use_m);

    let mut databases = database::database::load_multiple(
        matches.get_str_vec("database", None).iter(), &mut genome).unwrap_or_else(|e| panic!("{}", e));
    databases.sort_by(|a, b| b.database.total_len().cmp(&a.database.total_len()));
    if matches.contains("first") {
        let count_databases = matches.get_nth_value("first", 0);
        if count_databases != 0 {
            databases.truncate(count_databases);
            info!("Selected first {} databases", databases.len());
        }
    }
    if databases.is_empty() {
        error!("Loaded 0 databases. Stopping");
        return;
    }

    let db_names: Vec<String> = databases.iter().map(|db| db.name.clone()).collect();
    let db_indices: HashMap<String, u32> = db_names.iter().enumerate()
        .map(|(i, name)| (name.to_string(), i as u32)).collect();
    let mut narrator = Narrator::new(timer.clone());
    info!("Creating {} workers. Their logs are at {}/logs/worker<n>.log", threads,
        output.display());

    let read_psv_writer = if matches.contains("output-debug") {
        common::create_csv_gzip_writer(output.join("debug-reads.csv.gz"))
            .map_err(|e| error!("Could not open debug-reads.csv.gz: {}", e)).ok()
    } else {
        None
    };
    let psv_writer = if matches.contains("output-debug") {
        if let Ok(mut writer) = common::create_gzip_writer(output.join("debug-psvs.csv.gz"))
                .map_err(|e| error!("Could not open debug-psvs.csv.gz: {}", e)) {
            writeln!(writer, "iteration\tpsv\tactive\tpos1\tpos2\tprobs1\tprobs2\tsupport1\tsupport2\t\
                corr_probs1\tcorr_probs2\tgt1\tqual1\tgt2\tqual2\treliable")
                .expect("Failed to write to debug-psvs.csv.gz");
            Some(writer)
        } else {
            None
        }
    } else {
        None
    };
    let completed_dbs = get_completed_dbs(&output, &db_names, matches.contains("continue"));
    if !completed_dbs.is_empty() {
        info!("Skipping {} already completed components", completed_dbs.len());
    }
    let writer = rewrite_bam(&output, header.clone(), &completed_dbs)
        .unwrap_or_else(|e| panic!("Failed to create temporary BAM file: {}", e));
    let vcf_writer = rewrite_vcf(&output, &genome, &completed_dbs)
        .unwrap_or_else(|e| panic!("Failed to create temporary VCF file: {}", e));

    let mut pool = ThreadPool::new(
        (0..threads).map(|i| Worker::new(i, terminal_logger.clone(), &matches, params.clone(), id_converter.clone())));
    let database_observer = narrator.create_observer();
    let total_observer = narrator.create_observer();
    total_observer.prefix("Total databases:".to_string());
    total_observer.progress(0, databases.len() as u32);
    let narrator_handle = thread::Builder::new().name("narrator".to_string())
        .spawn(move || narrator.run(Duration::from_secs(1))).expect("Failed to create a thread");

    let mut args = MappingStructures {
        matches: &matches,
        params: &params,
        pool: &mut pool,
        observer: &database_observer,
        id_converter: &id_converter,
        reader,
        writer,
        genome: &mut genome,
        read_psv_writer,
        psv_writer,
        conflict_rate: None,
        vcf_writer,
    };
    let mut non_empty_names = Vec::with_capacity(db_names.len());
    for (name, db) in db_names.into_iter().zip(databases.into_iter()) {
        if completed_dbs.contains(&name) {
            debug!("Skipping component {} (already completed)", name);
            non_empty_names.push(name);
            total_observer.inc();
            continue;
        }
        if map_to_database(db, &mut args) {
            non_empty_names.push(name);
        }
        total_observer.inc();
    }
    std::mem::drop(args);
    database_observer.finish();
    total_observer.finish();

    pool.join();
    narrator_handle.join().expect("Stop due to panic in another thread");
    if non_empty_names.is_empty() {
        panic!("Did not find any reads in the duplicated regions. Stopping.");
    }

    info!("Sorting the file with the realigned reads");
    mapping::output_bam::sort(output, matches.get_str("samtools"), threads);
    let (read_names, conflicts) = {
        let mut changed_bam = bam::BamReader::from_path(output.join("realigned_reads.bam"), 0)
            .expect("Failed to open a temporary bam file with the realigned reads");
        mapping::output_bam::load_names(&mut changed_bam, &db_indices)
    };

    info!("Combining vcf and csv output files");
    if !matches.contains("skip-vcf") {
        mapping::output_vcf::sort(output, &genome)
            .unwrap_or_else(|e| error!("{}", e));
    }
    mapping::output_bam::merge_information(output, &non_empty_names, &conflicts, &db_indices)
        .unwrap_or_else(|e| error!("{}", e));
    mapping::output_bam::merge_csv(output, "window_cov.csv", &non_empty_names)
        .unwrap_or_else(|_| error!("Cannot join window_cov.csv files"));

    let mut initial_bam = bam::BamReader::from_path(matches.get_str("input"), 0)
        .expect("Failed to open input bam file or it has no bai index");
    let final_path = output.join("realigned.bam");
    let final_path = final_path.to_str().expect("Output path is not UTF-8");
    let mut writer = bam::BamWriter::build()
        .additional_threads(threads.saturating_sub(1))
        .from_path(final_path, header.clone())
        .expect("Could not create output file");

    let mut changed_bam = bam::BamReader::from_path(output.join("realigned_reads.bam"), 0)
        .expect("Failed to open a temporary bam file with the realigned reads");

    mapping::output_bam::combine_bams(&mut writer, &mut changed_bam, &mut initial_bam,
        matches.contains("skip-unique"), &read_names, &conflicts, &db_indices)
        .unwrap_or_else(|e| panic!("{}", e));
    std::mem::drop(changed_bam);
    std::mem::drop(initial_bam);
    writer.finish().expect("Failed to write output reads");
    std::mem::drop(writer);
    mapping::output_bam::index_bam(final_path, matches.get_str("samtools"), threads);

    if !matches.contains("keep") {
        info!("Cleaning output directory");
        for name in non_empty_names.iter() {
            fs::remove_dir_all(output.join(name))
                .unwrap_or_else(|_| error!("Failed to remove temporary directory {}", name));
        }
        fs::remove_file(output.join("realigned_reads.bam"))
            .unwrap_or_else(|_| error!("Failed to remove a temporary file realigned_reads.bam"));
    }
    common::goodbye(&timer.elapsed());
}
