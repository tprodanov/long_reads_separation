#!/usr/bin/env python3

import argparse
import socket
import sys
import os
import csv
import subprocess
import time
import math
import tqdm


def mkdir(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass


def save_xrandr_configuration():
    outp = subprocess.run(['xrandr', '-q'], check=True, capture_output=True)
    for line in outp.stdout.decode().split('\n'):
        if 'connected primary' in line:
            line = line.split()
            monitor = line[0]
            resolution = line[3].split('+')[0]
            panning = line[-1]
            if resolution.count('x') != 1 or panning.count('x') != 1 or panning.count('+') != 2:
                sys.stderr.write('Cannot parse xrandr answer: %s' % line)
                exit(1)
            return monitor, resolution, panning
    sys.stderr.write('Cannot find primary monitor\n')
    exit(1)


def change_xrandr_configuration(log, monitor, resolution, panning):
    command = ['xrandr', '--output', monitor, '--mode', resolution, '--panning', panning, '--scale', '1.0x1.0']
    log.write('Run %s\n' % ' '.join(command))
    outp = subprocess.run(command, capture_output=True)
    if outp.returncode != 0:
        log.write('Command failed with:    stdout: %s\n,    stderr: %s\n'
            % (outp.stdout.decode(), outp.stderr.decode()))


def run(s, log, *commands, sep=' '):
    command = sep.join(commands) + '\n'
    log.write(command)
    s.sendall(command.encode('utf-8'))
    try:
        data = s.recv(1024).decode().strip()
        if data != 'OK':
            sys.stderr.write('IGV error: %s\n' % data)
        log.write('    [IGV] %s' % data)
    except socket.timeout:
        log.write('    [IGV] Timeout\n')
        sys.stderr.write('Timeout error. Try reloading IGV\n')
        exit(1)


def convert_segmental_duplications(sd_path, out_path):
    with open(sd_path) as inp, open(out_path, 'w') as outp:
        reader = csv.DictReader(inp, delimiter='\t')
        outp.write('track name="Segmental duplications" itemRgb="On"\n')
        for row in reader:
            outp.write('%s\t%s\t%s\t' % (row['chrom'], row['chromStart'], row['chromEnd']))
            outp.write('%s:%s-%s\t' % (row['otherChrom'], row['otherStart'], row['otherEnd']))
            outp.write('%s\t%s\t' % (row['score'], '+' if row['strand'] == '+' else '-'))
            outp.write('%s\t%s\t' % (row['chromStart'], row['chromEnd']))
            seq_simil = float(row['fracMatch'])
            if seq_simil < 0.98:
                color = '153,153,153'
            elif seq_simil < 0.99:
                color = '255,192,33'
            else:
                color = '255,163,33'
            outp.write('%s\n' % color)


def connect_to_igv(args, log):
    log.write('Create socket: host = %s, port = %d\n' % (args.host, args.port))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((args.host, args.port))
    s.settimeout(30)
    run(s, log, 'snapshotDirectory', os.path.abspath(args.output))
    return s


def load_tracks(s, args, log):
    run(s, log, 'new')
    run(s, log, 'genome', os.path.abspath(args.reference))

    if not args.data:
        args.data = []
    for line in args.data:
        name, path = line.strip().split('=')
        path = path.strip()
        path = os.path.abspath(os.path.expanduser(path))

        if ':' in name:
            name, mode = name.rsplit(':', 1)
            name = name.strip()
            mode = mode.strip()
        else:
            name = name.strip()
            mode = None

        run(s, log, 'load', path, 'name="%s"' % name)
        if mode == 'e':
            mode = 'expand'
        elif mode == 's':
            mode = 'squish'
        elif mode == 'c':
            mode = 'collapse'
        if mode:
            run(s, log, mode, '"%s"' % name)

    if args.duplications_out:
        run(s, log, 'load', os.path.abspath(args.duplications_out))


def snapshot_regions(s, args, log):
    window = args.window
    regions = list(args.bed)
    for i, line in enumerate(regions):
        chrom, start, end = line.split('\t')[:3]
        start = int(start)
        end = int(end)
        print('Region %d/%d: %s:%d-%d' % (i + 1, len(regions), chrom, start + 1, end))

        n_windows = math.ceil((end - start) / window)
        curr_window = int(math.ceil((end - start) / n_windows))

        for i in tqdm.tqdm(range(start, end, curr_window)):
            region = '%s:%d-%d' % (chrom, i + 1, i + curr_window)
            run(s, log, 'goto', region)
            run(s, log, 'snapshot', region + '.png')


def main():
    parser = argparse.ArgumentParser(
        description='Take multiple screenshots of IGV',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -r <fasta> -b <bed> [-!] [-s <csv>] [-d <file>] -o <dir> [args]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-r', '--reference', metavar='FILE', required=True,
        help='Required: FASTA file with genome reference')
    io_args.add_argument('-b', '--bed', metavar='FILE', required=True, type=argparse.FileType(),
        help='Required: BED file with regions of interest')
    io_args.add_argument('-!', '--do-not-load', action='store_true',
        help='Do not load data or start new IGV session, just take multiple snapshots')
    io_args.add_argument('-s', '--seg-dups', metavar='CSV',
        help='Optional: CSV file with segmental duplications')
    io_args.add_argument('-d', '--data', metavar='FILE', type=argparse.FileType(),
        help='File with lines: "name[:mode] = path" to load into IGV.\n'
             'Name can contain spaces.\n'
             'Mode can be one of e|expand, s|squish, c|collapse.')
    io_args.add_argument('-o', '--output', metavar='FILE', required=True,
        help='Output directory with IGV snapshots')

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('-h', '--host', metavar='STR', default='127.0.0.1',
        help='IGV hostname [%(default)s]')
    opt_args.add_argument('-p', '--port', metavar='INT', type=int, default=60151,
        help='IGV port [%(default)s]')
    opt_args.add_argument('-R', '--resolution', metavar='INTxINT',
        help='Temporarily change resolution using xrandr')
    opt_args.add_argument('-w', '--window', metavar='INT', type=int, default=100,
        help='Window size [%(default)s]')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    s = None
    try:
        mkdir(args.output)
        log = open(os.path.join(args.output, 'log.txt'), 'w')

        if args.resolution:
            monitor, resolution, panning = save_xrandr_configuration()
            change_xrandr_configuration(log, monitor, resolution, args.resolution)
            time.sleep(2)
        else:
            monitor = None

        if args.seg_dups and not args.do_not_load:
            args.duplications_out = os.path.join(args.output, 'sd.bed')
            convert_segmental_duplications(args.seg_dups, args.duplications_out)

        s = connect_to_igv(args, log)
        if not args.do_not_load:
            load_tracks(s, args, log)

        snapshot_regions(s, args, log)

    except ConnectionRefusedError:
        sys.stderr.write('Could not connect to IGV. Try opening it')

    finally:
        if monitor is not None:
            change_xrandr_configuration(log, monitor, resolution, panning)
        if s is not None:
            s.close()


if __name__ == '__main__':
    main()
