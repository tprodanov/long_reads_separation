use bio::utils::TextSlice;
use slog_scope;
use slog::Drain;

use utils::common;

type AlignedPairs = Vec<(Option<u32>, Option<u32>)>;

struct Window {
    start_index: usize,
    end_index: usize,
    similarity: f64,
}

fn option_to_string<F: ToString>(x: &Option<F>) -> String {
    x.as_ref().map(|value| value.to_string()).unwrap_or_else(|| ".".to_string())
}

impl Window {
    fn new(start_index: usize, end_index: usize, errors: usize) -> Window {
        Window {
            start_index, end_index,
            similarity: 100.0 * 0.0_f64.max(1.0 - errors as f64 / (end_index - start_index) as f64),
        }
    }

    fn write_trace(&self, window_index: usize, aligned_pairs: &AlignedPairs) {
        let (start1, start2) = &aligned_pairs[self.start_index];
        let (end1, end2) = &aligned_pairs[self.end_index - 1];
        trace!("        Window {}: [{}-{}] [{}-{}] (similarity {:.1}%)",
            window_index,
            option_to_string(start1), option_to_string(end1),
            option_to_string(start2), option_to_string(end2),
            self.similarity);
    }
}

fn calculate_window_similarities(aligned_pairs: &AlignedPairs, seq1: TextSlice, seq2: TextSlice,
        window_size: usize) -> Vec<Window> {
    let start_index = aligned_pairs.iter().take_while(|(a, b)| a.is_none() || b.is_none()).count();
    let end_index = aligned_pairs.len()
        - aligned_pairs.iter().rev().take_while(|(a, b)| a.is_none() || b.is_none()).count();
    assert!(start_index < end_index, "Alignment has no matching positions");
    let window_size = common::adjust_window_size(end_index - start_index, window_size);
    let mut res = Vec::new();

    let mut current_start_index = start_index;
    let mut errors = 0;
    for i in start_index..end_index {
        errors += match &aligned_pairs[i] {
            (Some(pos1), Some(pos2)) => (seq1[*pos1 as usize] != seq2[*pos2 as usize]) as usize,
            (Some(_), None) => 1,
            (None, Some(_)) => 1,
            (None, None) => unreachable!(),
        };
        if (i - start_index) % window_size == window_size - 1 {
            res.push(Window::new(current_start_index, i + 1, errors));
            current_start_index = i + 1;
            errors = 0;
        }
    }
    if current_start_index < end_index {
        res.push(Window::new(current_start_index, end_index, errors));
    }
    res
}

fn trace_print_region(aligned_pairs: &AlignedPairs, start_index: usize, end_index: usize,
        similar: bool) {
    if !slog_scope::with_logger(|logger| logger.is_trace_enabled()) {
        return;
    }
    let (start1, start2) = &aligned_pairs[start_index];
    let (end1, end2) = &aligned_pairs[end_index - 1];
    trace!("    {} region : [{}-{}] [{}-{}]",
        if similar { "Similar" } else { "Diverse" },
        option_to_string(start1), option_to_string(end1),
        option_to_string(start2), option_to_string(end2));
}

fn split_on_regions(windows: &[Window], aligned_pairs: &AlignedPairs, min_windows: usize,
        min_similarity: f64) -> Vec<(usize, usize)> {
    let mut similar = true;
    let mut start_window = 0;
    let mut conflicting_windows = min_windows;
    let mut res = Vec::new();
    for (i, window) in windows.iter().enumerate() {
        if (window.similarity >= min_similarity) == similar {
            conflicting_windows = 0;
        } else {
            conflicting_windows += 1;
            if conflicting_windows >= min_windows {
                let end_window = i.saturating_sub(conflicting_windows);
                if end_window >= start_window {
                    let start_index = windows[start_window].start_index;
                    let end_index = windows[end_window].end_index;
                    trace_print_region(aligned_pairs, start_index, end_index, similar);
                    if similar {
                        res.push((start_index, end_index));
                    }
                    start_window = (i + 1).saturating_sub(conflicting_windows);
                }
                similar = !similar;
                conflicting_windows = 0;
            }
        }
    }
    let end_window = windows.len() - 1;
    let start_index = windows[start_window].start_index;
    let end_index = windows[end_window].end_index;
    trace_print_region(aligned_pairs, start_index, end_index, similar);
    if similar {
        res.push((start_index, end_index));
    }
    res
}

pub fn split_into_matching_pairs(aligned_pairs: &AlignedPairs, seq1: TextSlice, seq2: TextSlice,
        window_size: usize, min_windows: usize, min_similarity: f64) -> Vec<Vec<(u32, u32)>> {
    let windows = calculate_window_similarities(aligned_pairs, seq1, seq2, window_size);
    if slog_scope::with_logger(|logger| logger.is_trace_enabled()) {
        for (i, window) in windows.iter().enumerate() {
            window.write_trace(i, aligned_pairs);
        }
    }
    let similar_regions = split_on_regions(&windows, aligned_pairs, min_windows, min_similarity);
    similar_regions.into_iter().map(|(start_index, end_index)|
        aligned_pairs[start_index..end_index].iter()
            .filter_map(|pair| match pair {
                (Some(pos1), Some(pos2)) => Some((*pos1, *pos2)),
                _ => None,
            }).collect()
        ).collect()
}