use biology::spans::{Span, PositiveSpan};

pub struct SpanSet<T> {
    trees: Vec<iset::IntervalMap<u32, T>>,
}

impl<T> SpanSet<T> {
    pub fn new(n_chromosomes: usize) -> Self {
        Self {
            trees: (0..n_chromosomes).map(|_| iset::IntervalMap::new()).collect(),
        }
    }

    pub fn insert(&mut self, span: &impl Span, value: T) {
        self.trees[span.chrom_id() as usize].insert(span.start()..span.end(), value)
    }

    pub fn find(&self, span: &impl Span) -> impl Iterator<Item = &T> {
        self.trees[span.chrom_id() as usize].values(span.start()..span.end())
    }

    pub fn find_pairs(&self, span: &impl Span) -> impl Iterator<Item = (PositiveSpan, &T)> {
        let chrom_id = span.chrom_id();
        self.trees[chrom_id as usize].iter(span.start()..span.end())
            .map(move |(interval, value)| (PositiveSpan::new(chrom_id, interval.start, interval.end), value))
    }
}
